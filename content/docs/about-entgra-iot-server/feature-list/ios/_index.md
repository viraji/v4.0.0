---
bookCollapseSection: true
weight: 2
---

# iOS features

## Operations

Available operation for iOS and MacOS varies due to the way Apple defines the protocol and also varies depending on whether the device is a BYOD or a DEP(COPE) device.

<table class="tableizer-table">
<thead><tr class="tableizer-firstrow"><th></th><th>iOS</th><th>&nbsp;</th><th>MacOS </th><th>&nbsp;</th></tr></thead><tbody>
 <tr><td>Feature</td><td>BYOD</td><td>DEP</td><td>BYOD</td><td>Description</td></tr>
 <tr><td>Get Device Information</td><td>✓</td><td>✓</td><td>✓</td><td>Fetch the device's runtime information.</td></tr>
 <tr><td>Get Installed Applications</td><td>✓</td><td>✓</td><td>✓</td><td>Fetch the device's installed application list </td></tr>
 <tr><td>Get Device Location Information</td><td>✓</td><td>✓</td><td>✕</td><td>Fetch the device's current location</td></tr>
 <tr><td>Ring Device</td><td>✓</td><td>✓</td><td>✕</td><td>Ring the device for the purpose of locating the device in case of misplace.</td></tr>
 <tr><td>Install/Uninstall/update applications</td><td>✓</td><td>✓</td><td>✕</td><td>Manage apps installed on the device</td></tr>
 <tr><td>Enterprise data wipe</td><td>✓</td><td>✓</td><td>✓</td><td>Wipe the entreprise portion of the device</td></tr>
 <tr><td>Lock Device</td><td>✓</td><td>✓</td><td>✓</td><td>Lock the device remotely. Similar to pressing the power button on the device and locking it.</td></tr>
 <tr><td>Send Notification</td><td>✓</td><td>✓</td><td>✕</td><td>Send a notification(message) to the device</td></tr>
 <tr><td>Clear Passcode</td><td>✓</td><td>✓</td><td>✕</td><td>Clear the passcode of a mobile device</td></tr>
 <tr><td>Wipe Device(factory reset)</td><td>✓</td><td>✓</td><td>✓</td><td>Factory reset a device.</td></tr>
 <tr><td>Restart</td><td>✕</td><td>✓</td><td>✓</td><td>Restart command is issued to restart the device</td></tr>
 <tr><td>Shutdown</td><td>✕</td><td>✓</td><td>✓</td><td>Shutdown command is issued to shutdown the device</td></tr>
</tbody></table>



## Policies

The policies that can be applied on an iOS device depends on the way the device is enrolled with the server. 

<table class="tableizer-table">
    <thead>
        <tr class="tableizer-firstrow">
            <th>Feature</th>
            <th>BYOD</th>
            <th>DEP</th>
            <th>BYOD</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><a href="{{< param doclink>}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/virtual-private-network/">Virtual Private Network(VPN Settings)</a></td>
            <td>✓</td>
            <td>✓</td>
            <td>✓</td>
            <td>Push a configuration contaning the VPN profile of the company.</td>
        </tr>
        <tr>
            <td><a href="{{< param doclink>}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/wi-fi-policy/">Wi-Fi Settings</a></td>
            <td>✓</td>
            <td>✓</td>
            <td>✓</td>
            <td>Push a configuration contaning the wifi profile of the company.</td>
        </tr>
        <tr>
            <td><a href="{{< param doclink>}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/calender/">Calendar</a></td>
            <td>✓</td>
            <td>✓</td>
            <td>✓</td>
            <td>This payload configures a CalDAV account.</td>
        </tr>
        <tr>
            <td><a href="{{< param doclink>}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/calendar-subscription/">Calendar Subscription</a></td>
            <td>✓</td>
            <td>✓</td>
            <td>✕</td>
            <td>Adds a subscribed calendar to the userʼs calendars list.</td>
        </tr>
        <tr>
            <td><a href="{{< param doclink>}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/cellular-network-settings/">Cellular Network Settings</a></td>
            <td>✓</td>
            <td>✓</td>
            <td>✕</td>
            <td>Push cellular configurations such as APN settings to a mobile device</td>
        </tr>
        <tr>
            <td><a href="{{< param doclink>}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/email-settings/">Email settings</a></td>
            <td>✓</td>
            <td>✓</td>
            <td>✓</td>
            <td>These configurations can be used to define settings for connecting to your POP or IMAP email accounts.</td>
        </tr>
        <tr>
            <td><a href="{{< param doclink>}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/l-d-a-p-settings/">LDAP Settings</a></td>
            <td>✓</td>
            <td>✓</td>
            <td>✓</td>
            <td>These configurations can be used to define settings for connecting to your LDAP server.</td>
        </tr>
        <tr>
            <td><a href="{{< param doclink>}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/manage-domains/">Manage Domains</a></td>
            <td>✓</td>
            <td>✓</td>
            <td>✕</td>
            <td>Any document downloaded from the given URLs are marked as managed documents and will be used in managed open in restrictions.</td>
        </tr>
        <tr>
            <td><a href="{{< param doclink>}}">Unmarked Email Domains</a></td>
            <td>✓</td>
            <td>✓</td>
            <td>✓</td>
            <td>Specify a list of email domains that are enterprise recognised so that the others are marked as unregnised by highlighting in the mail client</td>
        </tr>
        <tr>
            <td><a href="{{< param doclink>}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/passcode-policy/">Passcode policy</a></td>
            <td>✓</td>
            <td>✓</td>
            <td>✓</td>
            <td>Add a passcode strength policy to the device or to work profile</td>
        </tr>
        <tr>
            <td><a href="{{< param doclink>}}">Monitor/Revoke Policies</a></td>
            <td>✓</td>
            <td>✓</td>
            <td>✓</td>
            <td>Continiously monitor the policies of the device to detect any policy violations.</td>
        </tr>
        <tr>
            <td><a href="{{< param doclink>}}"guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/certificate-install/>Certificate install</a></td>
            <td>✓</td>
            <td>✓</td>
            <td>&nbsp;</td>
            <td>Install certificate to devices remotely</td>
        </tr>
        <tr>
            <td><a href="{{< param doclink>}}"guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/global-proxy-settings/>Global Proxy Settings</a></td>
            <td>✓</td>
            <td>✓</td>
            <td>&nbsp;</td>
            <td>Reroute all the http communication of a device via a global http proxy</td>
        </tr>
        <tr>
            <td><a href="{{< param doclink>}}">Disallow removal of profile</a></td>
            <td>✕</td>
            <td>✓</td>
            <td>✕</td>
            <td>Disable the user's ability to unenroll from EMM</td>
        </tr>
        <tr>
            <td><a href="{{< param doclink>}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/air-play-settings/">AirPlay Settings</a></td>
            <td>✓</td>
            <td>✓</td>
            <td>✕</td>
            <td>The AirPrint payload adds AirPrint printers to the user's AirPrint printer list.</td>
        </tr>
        <tr>
            <td><a href="{{< param doclink>}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/network-usage-rules/">Network Usage Rules</a></td>
            <td>✕</td>
            <td>✓</td>
            <td>✕</td>
            <td>Network Usage Rules allow enterprises to specify how managed apps use networks, such as cellular data networks</td>
        </tr>
        <tr>
            <td><a href="{{< param doclink>}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/app-lock/">App Lock(Kiosk mode)</a></td>
            <td>✕</td>
            <td>✓</td>
            <td>✕</td>
            <td>Configure the behaviour of the Kiosk</td>
        </tr>
        <tr>
            <td><a href="{{< param doclink>}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/font-install/">Font install</a></td>
            <td>✕</td>
            <td>✓</td>
            <td>✕</td>
            <td>Install fonts to an iOS device remotely</td>
        </tr>
        <tr>
            <td><a href="{{< param doclink>}}">Exchange</a></td>
            <td>✕</td>
            <td>✓</td>
            <td>✓</td>
            <td>Exchange active sync contacts and mails to devices</td>
        </tr>
        <tr>
            <td><a href="{{< param doclink>}}">Managed Settings command</a></td>
            <td>✕</td>
            <td>✓</td>
            <td>✓</td>
            <td>Send the app configurations for user's installed apps</td>
        </tr>
        <tr>
            <td><a href="{{< param doclink>}}">AppStore Payload</a></td>
            <td>✕</td>
            <td>✕</td>
            <td>✓</td>
            <td>Enforce restrictions on the App store in macOS</td>
        </tr>
        <tr>
            <td><a href="{{< param doclink>}}">Loginwindow Payload</a></td>
            <td>✕</td>
            <td>✕</td>
            <td>✓</td>
            <td>Behaviour of the login screen and users are controlled with this policy</td>
        </tr>
        <tr>
            <td><a href="{{< param doclink>}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/firewall-policy/">Firewall Policy</a></td>
            <td>✕</td>
            <td>✕</td>
            <td>✓</td>
            <td>A Firewall payload manages the Application Firewall settings that are accessible in the Security Preferences pane.</td>
        </tr>
    </tbody>
</table>

### <a href="{{< param doclink>}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/restrictions/"> Restrictions Policy </a>

here are large number of restrictions that can be applied on the device. Restrictions are a policy and following describes all the restrictions available.

<table class="tableizer-table">
<thead><tr class="tableizer-firstrow"><th>allowAppRemoval</th><th>✓</th><th>✓</th><th>✕</th><th>When false, disables removal of apps from iOS device. This key is deprecated on unsupervised devices.</th></tr></thead><tbody>
 <tr><td>allowAssistant</td><td>✓</td><td>✓</td><td>✕</td><td>When false, disables Siri. Defaults to true.</td></tr>
 <tr><td>allowAssistantWhileLocked</td><td>✓</td><td>✓</td><td>✕</td><td>"When false, the user is unable to use Siri when the device is locked. Defaults to true. This restriction is ignored if the device does not have a passcode set.</td></tr>
 <tr><td>Availability: Available only in iOS 5.1 and later."</td></tr>
 <tr><td>allowCamera</td><td>✓</td><td>✓</td><td>✓</td><td>"When false, the camera is completely disabled and its icon is removed from the Home screen. Users are unable to take photographs.</td></tr>
 <tr><td>Availability: Available in iOS and in macOS 10.11 and later."</td></tr>
 <tr><td>allowCloudDocumentSync</td><td>✓</td><td>✓</td><td>✓</td><td>"When false, disables document and key-value syncing to iCloud. This key is deprecated on unsupervised devices.</td></tr>
 <tr><td>Availability: Available in iOS 5.0 and later and in macOS 10.11 and later."</td></tr>
 <tr><td>allowCloudKeychainSync</td><td>✓</td><td>✓</td><td>✓</td><td>"When false, disables iCloud keychain synchronization. Default is true.</td></tr>
 <tr><td>Availability: Available in iOS 7.0 and later and macOS 10.12 and later."</td></tr>
 <tr><td>allowDiagnosticSubmission</td><td>✓</td><td>✓</td><td>✕</td><td>"When false, this prevents the device from automatically submitting diagnostic reports to Apple. Defaults to true.</td></tr>
 <tr><td>Availability: Available only in iOS 6.0 and later."</td></tr>
 <tr><td>allowExplicitContent</td><td>✓</td><td>✓</td><td>✕</td><td>When false, explicit music or video content purchased from the iTunes Store is hidden. Explicit content is marked as such by content providers, such as record labels, when sold through the iTunes Store. This key is deprecated on unsupervised devices.</td></tr>
 <tr><td>allowFingerprintForUnlock</td><td>✓</td><td>✓</td><td>✓</td><td>"If false, prevents Touch ID from unlocking a device.</td></tr>
 <tr><td>Availability: Available in iOS 7 and later and in macOS 10.12.4 and later."</td></tr>
 <tr><td>allowGlobalBackgroundFetchWhenRoaming</td><td>✓</td><td>✓</td><td>✕</td><td>When false, disables global background fetch activity when an iOS phone is roaming.</td></tr>
 <tr><td>allowLockScreenNotificationsView</td><td>✓</td><td>✓</td><td>✕</td><td>"If set to false, the Notifications view in Notification Center on the lock screen is disabled and users can't receive notifications when the screen is locked.</td></tr>
 <tr><td>Availability: Available only in iOS 7.0 and later."</td></tr>
 <tr><td>allowLockScreenTodayView</td><td>✓</td><td>✓</td><td>✕</td><td>"If set to false, the Today view in Notification Center on the lock screen is disabled.</td></tr>
 <tr><td>Availability: Available only in iOS 7.0 and later."</td></tr>
 <tr><td>allowMultiplayerGaming</td><td>✓</td><td>✓</td><td>✕</td><td>When false, prohibits multiplayer gaming. This key is deprecated on unsupervised devices.</td></tr>
 <tr><td>allowOpenFromManagedToUnmanaged</td><td>✓</td><td>✓</td><td>✕</td><td>"If false, documents in managed apps and accounts only open in other managed apps and accounts. Default is true.</td></tr>
 <tr><td>Availability: Available only in iOS 7.0 and later."</td></tr>
 <tr><td>allowOpenFromUnmanagedToManaged</td><td>✓</td><td>✓</td><td>✕</td><td>"If set to false, documents in unmanaged apps and accounts will only open in other unmanaged apps and accounts. Default is true.</td></tr>
 <tr><td>Availability: Available only in iOS 7.0 and later."</td></tr>
 <tr><td>allowScreenShot</td><td>✓</td><td>✓</td><td>✕</td><td>"If set to false, users can't save a screenshot of the display and are prevented from capturing a screen recording; it also prevents the Classroom app from observing remote screens. Defaults to true.</td></tr>
 <tr><td>Availability: Updated in iOS 9.0 to include screen recordings."</td></tr>
 <tr><td>allowPhotoStream</td><td>✓</td><td>✓</td><td>✕</td><td>"When false, disables Photo Stream.</td></tr>
 <tr><td>Availability: Available in iOS 5.0 and later."</td></tr>
 <tr><td>allowSafari</td><td>✓</td><td>✓</td><td>✕</td><td>When false, the Safari web browser application is disabled and its icon removed from the Home screen. This also prevents users from opening web clips. This key is deprecated on unsupervised devices.</td></tr>
 <tr><td>safariAllowAutoFill</td><td>✓</td><td>✓</td><td>✕</td><td>When false, Safari auto-fill is disabled. Defaults to true.</td></tr>
 <tr><td>safariForceFraudWarning</td><td>✓</td><td>✓</td><td>✕</td><td>When true, Safari fraud warning is enabled. Defaults to false.</td></tr>
 <tr><td>safariAllowJavaScript</td><td>✓</td><td>✓</td><td>✕</td><td>When false, Safari will not execute JavaScript. Defaults to true.</td></tr>
 <tr><td>safariAllowPopups</td><td>✓</td><td>✓</td><td>✕</td><td>When false, Safari will not allow pop-up tabs. Defaults to true.</td></tr>
 <tr><td>safariAcceptCookies</td><td>✓</td><td>✓</td><td>✕</td><td>"Determines conditions under which the device will accept cookies.</td></tr>
 <tr><td>The user facing settings changed in iOS 11, though the possible values remain the same:</td></tr>
 <tr><td>"</td></tr>
 <tr><td>allowSharedStream</td><td>✓</td><td>✓</td><td>✕</td><td>"If set to false, Shared Photo Stream will be disabled. This will default to true.</td></tr>
 <tr><td>Availability: Available in iOS 6.0 and later."</td></tr>
 <tr><td>allowUntrustedTLSPrompt</td><td>✓</td><td>✓</td><td>✕</td><td>"When false, automatically rejects untrusted HTTPS certificates without prompting the user.</td></tr>
 <tr><td>Availability: Available in iOS 5.0 and later."</td></tr>
 <tr><td>allowVideoConferencing</td><td>✓</td><td>✓</td><td>✕</td><td>When false, disables video conferencing. This key is deprecated on unsupervised devices.</td></tr>
 <tr><td>allowVoiceDialing</td><td>✓</td><td>✓</td><td>✕</td><td>When false, disables voice dialing if the device is locked with a passcode. Default is true.</td></tr>
 <tr><td>allowYouTube</td><td>✓</td><td>✓</td><td>✕</td><td>"When false, the YouTube application is disabled and its icon is removed from the Home screen.</td></tr>
 <tr><td>This key is ignored in iOS 6 and later because the YouTube app is not provided."</td></tr>
 <tr><td>allowiTunes</td><td>✓</td><td>✓</td><td>✕</td><td>When false, the iTunes Music Store is disabled and its icon is removed from the Home screen. Users cannot preview, purchase, or download content. This key is deprecated on unsupervised devices.</td></tr>
 <tr><td>allowEnterpriseAppTrust</td><td>✓</td><td>✓</td><td>✕</td><td>"If set to false removes the Trust Enterprise Developer button in Settings->General->Profiles & Device Management, preventing apps from being provisioned by universal provisioning profiles. This restriction applies to free developer accounts but it does not apply to enterprise app developers who are trusted because their apps were pushed via MDM, nor does it revoke previously granted trust. Defaults to true.</td></tr>
 <tr><td>Availability: Available in iOS 9.0 and later. "</td></tr>
 <tr><td>forceEncryptedBackup</td><td>✓</td><td>✓</td><td>✕</td><td>When true, encrypts all backups.</td></tr>
 <tr><td>forceITunesStorePasswordEntry</td><td>✓</td><td>✓</td><td>✕</td><td>"When true, forces user to enter their iTunes password for each transaction.</td></tr>
 <tr><td>Availability: Available in iOS 5.0 and later."</td></tr>
 <tr><td>forceLimitAdTracking</td><td>✓</td><td>✓</td><td>✕</td><td>"If true, limits ad tracking. Default is false.</td></tr>
 <tr><td>Availability: Available only in iOS 7.0 and later."</td></tr>
 <tr><td>forceAirPlayOutgoingRequestsPairingPassword</td><td>✓</td><td>✓</td><td>✕</td><td>"If set to true, forces all devices receiving AirPlay requests from this device to use a pairing password. Default is false.</td></tr>
 <tr><td>Availability: Available only in iOS 7.1 and later."</td></tr>
 <tr><td>forceAirPlayIncomingRequestsPairingPassword</td><td>✓</td><td>✓</td><td>✕</td><td>"If set to true, forces all devices sending AirPlay requests to this device to use a pairing password. Default is false.</td></tr>
 <tr><td>Availability: Available only in Apple TV 6.1 and later."</td></tr>
 <tr><td>allowActivityContinuation</td><td>✓</td><td>✓</td><td>✕</td><td>If set to false, Activity Continuation will be disabled. Defaults to true.</td></tr>
 <tr><td>allowEnterpriseBookBackup</td><td>✓</td><td>✓</td><td>✕</td><td>If set to false, Enterprise books will not be backed up. Defaults to true.</td></tr>
 <tr><td>allowEnterpriseBookMetadataSync</td><td>✓</td><td>✓</td><td>✕</td><td>If set to false, Enterprise books notes and highlights will not be synced. Defaults to true.</td></tr>
 <tr><td>allowCloudPhotoLibrary</td><td>✓</td><td>✓</td><td>✓</td><td>"If set to false, disables iCloud Photo Library. Any photos not fully downloaded from iCloud Photo Library to the device will be removed from local storage.</td></tr>
 <tr><td>Availability: Available in iOS 9.0 and later and in macOS 10.12 and later.  "</td></tr>
 <tr><td>forceAirDropUnmanaged</td><td>✓</td><td>✓</td><td>✕</td><td>"If set to true, causes AirDrop to be considered an unmanaged drop target. Defaults to false.</td></tr>
 <tr><td>Availability: Available in iOS 9.0 and later. "</td></tr>
 <tr><td>forceWatchWristDetection</td><td>✓</td><td>✓</td><td>✕</td><td>"If set to true, a paired Apple Watch will be forced to use Wrist Detection. Defaults to false.</td></tr>
 <tr><td>Availability: Available in iOS 8.2 and later. "</td></tr>
 <tr><td>allowAddingGameCenterFriends</td><td>✓</td><td>✓</td><td>✕</td><td>When false, prohibits adding friends to Game Center. This key is deprecated on unsupervised devices.</td></tr>
 <tr><td>allowInAppPurchases</td><td>✓</td><td>✓</td><td>✕</td><td>When false, prohibits in-app purchasing.</td></tr>
 <tr><td>allowLockScreenControlCenter</td><td>✓</td><td>✓</td><td>✕</td><td>"If false, prevents Control Center from appearing on the Lock screen.</td></tr>
 <tr><td>Availability: Available in iOS 7 and later."</td></tr>
 <tr><td>allowOTAPKIUpdates</td><td>✓</td><td>✓</td><td>✕</td><td>"If false, over-the-air PKI updates are disabled. Setting this restriction to false does not disable CRL and OCSP checks. Default is true.</td></tr>
 <tr><td>Availability: Available only in iOS 7.0 and later."</td></tr>
 <tr><td>allowPassbookWhileLocked</td><td>✓</td><td>✓</td><td>✕</td><td>"If set to false, Passbook notifications will not be shown on the lock screen.This will default to true.</td></tr>
 <tr><td>Availability: Available in iOS 6.0 and later."</td></tr>
 <tr><td>allowManagedAppsCloudSync</td><td>✓</td><td>✓</td><td>✕</td><td>If set to false, prevents managed applications from using iCloud sync.</td></tr>
 <tr><td>allowPodcasts</td><td>✕</td><td>✓</td><td>✕</td><td>"Not available. Need DEP. If set to false, disables podcasts. Defaults to true.</td></tr>
 <tr><td>Availability: Available in iOS 8.0 and later. "</td></tr>
 <tr><td>allowDefinitionLookup</td><td>✕</td><td>✓</td><td>✓</td><td>"Not available. Need DEP. If set to false, disables definition lookup. Defaults to true.</td></tr>
 <tr><td>Availability: Available in iOS 8.1.3 and later and in macOS 10.11.2 and later. "</td></tr>
 <tr><td>allowPredictiveKeyboard</td><td>✕</td><td>✓</td><td>✕</td><td>"Not available. Need DEP. If set to false, disables predictive keyboards. Defaults to true.</td></tr>
 <tr><td>Availability: Available in iOS 8.1.3 and later. "</td></tr>
 <tr><td>allowAutoCorrection</td><td>✕</td><td>✓</td><td>✕</td><td>"Not available. Need DEP. If set to false, disables keyboard auto-correction. Defaults to true.</td></tr>
 <tr><td>Availability: Available in iOS 8.1.3 and later. "</td></tr>
 <tr><td>allowSpellCheck</td><td>✕</td><td>✓</td><td>✕</td><td>"Not available. Need DEP. If set to false, disables keyboard spell-check. Defaults to true.</td></tr>
 <tr><td>Availability: Available in iOS 8.1.3 and later. "</td></tr>
 <tr><td>allowMusicService</td><td>✕</td><td>✓</td><td>✓</td><td>"Not available. Need DEP. If set to false, Music service is disabled and Music app reverts to classic mode. Defaults to true.</td></tr>
 <tr><td>Availability: Available in iOS 9.3 and later and macOS 10.12 and later. "</td></tr>
 <tr><td>allowNews</td><td>✕</td><td>✓</td><td>✕</td><td>"Not available. Need DEP. If set to false, disables News. Defaults to true.</td></tr>
 <tr><td>Availability: Available in iOS 9.0 and later. "</td></tr>
 <tr><td>allowUIAppInstallation</td><td>✕</td><td>✓</td><td>✕</td><td>"When false, the App Store is disabled and its icon is removed from the Home screen. However, users may continue to use Host apps (iTunes, Configurator) to install or update their apps. Defaults to true.</td></tr>
 <tr><td>Availability: Available in iOS 9.0 and later. "</td></tr>
 <tr><td>allowKeyboardShortcuts</td><td>✕</td><td>✓</td><td>✕</td><td>"If set to false, keyboard shortcuts cannot be used. Defaults to true.</td></tr>
 <tr><td>Availability: Available in iOS 9.0 and later. "</td></tr>
 <tr><td>allowPairedWatch</td><td>✕</td><td>✓</td><td>✕</td><td>"If set to false, disables pairing with an Apple Watch. Any currently paired Apple Watch is unpaired and erased. Defaults to true.</td></tr>
 <tr><td>Availability: Available in iOS 9.0 and later. "</td></tr>
 <tr><td>allowPasscodeModification</td><td>✕</td><td>✓</td><td>✕</td><td>"If set to false, prevents the device passcode from being added, changed, or removed. Defaults to true. This restriction is ignored by shared iPads.</td></tr>
 <tr><td>Availability: Available in iOS 9.0 and later. "</td></tr>
 <tr><td>allowDeviceNameModification</td><td>✕</td><td>✓</td><td>✕</td><td>"If set to false, prevents device name from being changed. Defaults to true.</td></tr>
 <tr><td>Availability: Available in iOS 9.0 and later. "</td></tr>
 <tr><td>allowWallpaperModification</td><td>✕</td><td>✓</td><td>✕</td><td>"If set to false, prevents wallpaper from being changed. Defaults to true.</td></tr>
 <tr><td>Availability: Available in iOS 9.0 and later. "</td></tr>
 <tr><td>allowAutomaticAppDownloads</td><td>✕</td><td>✓</td><td>✕</td><td>"If set to false, prevents automatic downloading of apps purchased on other devices. Does not affect updates to existing apps. Defaults to true.</td></tr>
 <tr><td>Availability: Available in iOS 9.0 and later. "</td></tr>
 <tr><td>allowRadioService</td><td>✕</td><td>✓</td><td>✕</td><td>"If set to false, Apple Music Radio is disabled. Defaults to true.</td></tr>
 <tr><td>Availability: Available in iOS 9.3 and later. "</td></tr>
 <tr><td>blacklistedAppBundleIDs</td><td>✕</td><td>✓</td><td>✕</td><td>"If present, prevents bundle IDs listed in the array from being shown or launchable.</td></tr>
 <tr><td>Availability: Available in iOS 9.3 and later. "</td></tr>
 <tr><td>whitelistedAppBundleIDs</td><td>✕</td><td>✓</td><td>✕</td><td>"If present, allows only bundle IDs listed in the array from being shown or launchable.</td></tr>
 <tr><td>Availability: Available in iOS 9.3 and later."</td></tr>
 <tr><td>allowNotificationsModification</td><td>✕</td><td>✓</td><td>✕</td><td>"If set to false, notification settings cannot be modified. Defaults to true.</td></tr>
 <tr><td>Availability: Available in iOS 9.3 and later. "</td></tr>
 <tr><td>allowRemoteScreenObservation</td><td>✕</td><td>✓</td><td>✕</td><td>"If set to false, remote screen observation by the Classroom app is disabled. Defaults to true.</td></tr>
 <tr><td>This key should be nested beneath allowScreenShot as a sub-restriction. If allowScreenShot is set to false, it also prevents the Classroom app from observing remote screens. Availability: Available in iOS 9.3 and later. "</td></tr>
 <tr><td>allowDiagnosticSubmissionModification</td><td>✕</td><td>✓</td><td>✕</td><td>"If set to false, the diagnostic submission and app analytics settings in the Diagnostics & Usage pane in Settings cannot be modified. Defaults to true.</td></tr>
 <tr><td>Availability: Available in iOS 9.3.2 and later. "</td></tr>
 <tr><td>allowBluetoothModification</td><td>✕</td><td>✓</td><td>✕</td><td>"If set to false, prevents modification of Bluetooth settings. Defaults to true.</td></tr>
 <tr><td>Availability: Available in iOS 10.0 and later. "</td></tr>
 <tr><td>allowDictation</td><td>✕</td><td>✓</td><td>✕</td><td>"If set to false, disallows dictation input. Defaults to true.</td></tr>
 <tr><td>Availability: Available only in iOS 10.3 and later. "</td></tr>
 <tr><td>forceWiFiWhitelisting</td><td>✕</td><td>✓</td><td>✕</td><td>"If set to true, the device can join Wi-Fi networks only if they were set up through a configuration profile. Defaults to false.</td></tr>
 <tr><td>Availability: Available only in iOS 10.3 and later. "</td></tr>
 <tr><td>forceUnpromptedManaged- ClassroomScreenObservation</td><td>✕</td><td>✕</td><td>✕</td><td>"If set to true, and ScreenObservationPermissionModificationAllowed is also true in the Education payload, a student enrolled in a managed course via the Classroom app will automatically give permission to that course's teacher's requests to observe the student's screen without prompting the student. Defaults to false.</td></tr>
 <tr><td>Availability: Available only in iOS 10.3 and later. "</td></tr>
 <tr><td>allowAirPrint</td><td>✕</td><td>✓</td><td>✓</td><td>"If set to false, disallow AirPrint. Defaults to true.</td></tr>
 <tr><td>Availability: Available only in iOS 11.0 and macOS 10.13 and later. "</td></tr>
 <tr><td>allowAirPrintCredentialsStorage</td><td>✕</td><td>✓</td><td>✕</td><td>"If set to false, disallows keychain storage of username and password for Airprint. Defaults to true.</td></tr>
 <tr><td>Availability: Available only in iOS 11.0 and later. "</td></tr>
 <tr><td>forceAirPrintTrustedTLSRequirement</td><td>✕</td><td>✓</td><td>✓</td><td>"If set to true, requires trusted certificates for TLS printing communication. Defaults to false.</td></tr>
 <tr><td>Availability: Available only in iOS 11.0 and macOS 10.13 and later. "</td></tr>
 <tr><td>allowAirPrintiBeaconDiscovery</td><td>✕</td><td>✓</td><td>✓</td><td>"If set to false, disables iBeacon discovery of AirPrint printers. This prevents spurious AirPrint Bluetooth beacons from phishing for network traffic. Defaults to true.</td></tr>
 <tr><td>Availability: Available only in iOS 11.0 and macOS 10.13 and later. "</td></tr>
 <tr><td>allowSystemAppRemoval</td><td>✕</td><td>✓</td><td>✕</td><td>"If set to false, disables the removal of system apps from the device. Defaults to true.</td></tr>
 <tr><td>Availability: Available only in iOS 11.0 and later. "</td></tr>
 <tr><td>allowVPNCreation</td><td>✕</td><td>✓</td><td>✕</td><td>"If set to false, disallow the creation of VPN configurations. Defaults to true.</td></tr>
 <tr><td>Availability: Available only in iOS 11.0 and later. "</td></tr>
 <tr><td>allowProximitySetupToNewDevice</td><td>✕</td><td>✓</td><td>✕</td><td>"Supervised only. If set to false, disables the prompt to setup new devices that are nearby. Defaults to true.</td></tr>
 <tr><td>Availability: Available only in iOS 11.0 and later. "</td></tr>
 <tr><td>allowAccountModification</td><td>✕</td><td>✓</td><td>✕</td><td>"If set to false, account modification is disabled.</td></tr>
 <tr><td>Availability: Available only in iOS 7.0 and later. "</td></tr>
 <tr><td>allowAirDrop</td><td>✕</td><td>✓</td><td>✕</td><td>"If set to false, AirDrop is disabled.</td></tr>
 <tr><td>Availability: Available only in iOS 7.0 and later. "</td></tr>
 <tr><td>allowAppCellularDataModification</td><td>✕</td><td>✓</td><td>✕</td><td>"If set to false, changes to cellular data usage for apps are disabled.</td></tr>
 <tr><td>Availability: Available only in iOS 7.0 and later. "</td></tr>
 <tr><td>allowAppInstallation</td><td>✕</td><td>✓</td><td>✕</td><td>When false, the App Store is disabled and its icon is removed from the Home screen. Users are unable to install or update their applications. This key is deprecated on unsupervised devices.</td></tr>
 <tr><td>allowAssistantUserGeneratedContent</td><td>✕</td><td>✓</td><td>✕</td><td>"When false, prevents Siri from querying user-generated content from the web.</td></tr>
 <tr><td>Availability: Available in iOS 7 and later. "</td></tr>
 <tr><td>allowBookstore</td><td>✕</td><td>✓</td><td>✕</td><td>"If set to false, iBookstore will be disabled. This will default to true.</td></tr>
 <tr><td>Availability: Available in iOS 6.0 and later. "</td></tr>
 <tr><td>allowBookstoreErotica</td><td>✕</td><td>✓</td><td>✕</td><td>"Not available prior to iOS 6.1. If set to false, the user will not be able to download media from the iBookstore that has been tagged as erotica. This will default to true.</td></tr>
 <tr><td>Availability: Available in iOS 6.0 and later. "</td></tr>
 <tr><td>allowChat</td><td>✕</td><td>✓</td><td>✕</td><td>"When false, disables the use of the Messages app with supervised devices.</td></tr>
 <tr><td>Availability: Available in iOS 6.0 and later. "</td></tr>
 <tr><td>allowFindMyFriendsModification</td><td>✕</td><td>✓</td><td>✕</td><td>"If set to false, changes to Find My Friends are disabled.</td></tr>
 <tr><td>Availability: Available only in iOS 7.0 and later. "</td></tr>
 <tr><td>allowGameCenter</td><td>✕</td><td>✓</td><td>✕</td><td>"When false, Game Center is disabled and its icon is removed from the Home screen. Default is true.</td></tr>
 <tr><td>Availability: Available only in iOS 6.0 and later. "</td></tr>
 <tr><td>allowHostPairing</td><td>✕</td><td>✓</td><td>✕</td><td>"If set to false, host pairing is disabled with the exception of the supervision host. If no supervision host certificate has been configured, all pairing is disabled. Host pairing lets the administrator control which devices an iOS 7 device can pair with.</td></tr>
 <tr><td>Availability: Available only in iOS 7.0 and later. "</td></tr>
 <tr><td>allowUIConfigurationProfileInstallation</td><td>✕</td><td>✓</td><td>✕</td><td>"If set to false, the user is prohibited from installing configuration profiles and certificates interactively. This will default to true.</td></tr>
 <tr><td>Availability: Available in iOS 6.0 and later. "</td></tr>
 <tr><td>autonomousSingleAppModePermittedAppIDs</td><td>✕</td><td>✕</td><td>✕</td><td>"If present, allows apps identified by the bundle IDs listed in the array to autonomously enter Single App Mode.</td></tr>
 <tr><td>Availability: Available only in iOS 7.0 and later."</td></tr>
 <tr><td>forceAssistantProfanityFilter</td><td>✕</td><td>✓</td><td>✕</td><td>When true, forces the use of the profanity filter assistant. </td></tr>
 <tr><td>allowEraseContentAndSettings</td><td>✕</td><td>✓</td><td>✕</td><td>If set to false, disables the "Erase All Content And Settings" option in the Reset UI. </td></tr>
 <tr><td>allowSpotlightInternetResults</td><td>✕</td><td>✓</td><td>✓</td><td>"If set to false, Spotlight will not return Internet search results.</td></tr>
 <tr><td>Availability: Available in iOS and in macOS 10.11 and later. "</td></tr>
 <tr><td>allowEnablingRestrictions</td><td>✕</td><td>✓</td><td>✕</td><td>If set to false, disables the "Enable Restrictions" option in the Restrictions UI in Settings.</td></tr>
</tbody></table>
