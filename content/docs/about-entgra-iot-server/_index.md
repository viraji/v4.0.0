---
bookCollapseSection: true
weight: 1
---
# About Entgra IoT Server

This section briefly describes the architecture of the Entgra IoT server.

Entgra’s IoT Solution is leading the way in harnessing the best of  IoT potential and empowering solutions that are sought after for a variety of rapidly changing requirements in IoT. Powered by WSO2 technology, built on extensible plug-in architecture and enhanced by world-class engineering expertise, the Entgra IoT server is capable of empowering the best of Enterprise Mobility Management (EMM) services available today.

Based on the need for enterprises to manage their mobile and IoT devices, developed to  industry standard REST APIs, the present platform has evolved through customer requirements and feedback to what it is today. Entgra is the only open source vendor as at now to support all features for Android and iOS in the same platform, along with IoT capabilities. 

Fully accomplished and on par with the market leaders of EMM (Enterprise Mobility Management) suites, the Entgra IoT server constitutes of the essential requirements of an ideal EMM suite as specified by the Gartner Magic Quadrant 2019*2 which are complete MDM, MAM, Containment, Mobile Identity and UEM capabilities. 

Some of the core features of Entgra IoT that distinctly makes it stand out are:
 
 * Security: of both devices and data.
 
 * Scalability: Ability to handle any number of devices and scale-up
 
 * Extensibility: Plug-in architecture
 
 * Multi-tenancy Architecture
 
 * Open Source: Freely available and flexible for commercializing 
 
 * Comprehensive Unified Endpoint Management (UEM) capabilities
 
 * Google Certified Enterprise Partner*3
 
 The Entgra IoT server comprises of the following broad areas of management:
 
 EMM - Enterprise Mobility Management
  
 MDM - Mobile Device Management
 
 MAM - Mobile Application Management
 
 Enterprise Mobility Management  (EMM)
 
 The EMM suite of Entgra’s IoT server comprises of extensive Device Management, Policy Management and Certificate Management capabilities. 

 <img src = "../image/3001.jpg" border="1" align="center" style="border:5px solid black ">
 
## MDM
 
 
 Resolving complex field force management scenarios with simple, time and cost effective solutions, Entgra’s MDM is easily customizable as per the requirements. 

 * Handles Complete MDM Lifecycle 
 * Focuses on monitoring, controlling, securing and enforcing policies on devices
 * Implementation of comprehensive and customized policy management solutions
 
## MAM
 
 * Mobile Application Management(MAM) includes application lifecycle management and securely deploying applications on user devices
 * Application store provides the capabilities to install/uninstall or upgrade applications on user’s devices.
 * Allows the management of corporate apps well as public apps from Google play store or Apple app store.
 * App management includes managing licenses, bulk license provisioning and scheduled app management.
 
## Functionality
 
 The functionality of the Entgra IoT server can be broadly classified into Core, Extended and Analytics capabilities as follows:
 
 **Core:** 
 
 Included in the IoT core functionality are its extensive device management capabilities covering all aspects of policy/configuration/operation and user management sections. This in effect is centralized around device management focusing on device plugins, event stream management and more.
 
 **Extended:**
 
 Entgra IoT server can be extended to be used with the integration, machine learning, workflows and many other areas.  
 
 **Analytics:**
 
 Extensive real-time, bach and edge analytics capabilities are provided by the Entgra IoT. 

## Entgra’s Edge (Our Competitive Advantage)

* Based on WSO2 platform - fully integrated IoT security, IS for Identity Federation
* Event Processing - Real-time, batch and edge analytics capability with real-time event processor. 
* Open source platform with full flexibility to commercialize individual projects.
* OEM Partnership to embed WSO2 Technologies
* Cloud and on-premise deployments, easily migratable from one to another.
* Ultimate adaptability -  a multitude of control options and extension points 
* Enterprise grade architecture
* Battle-tested platform in over global 2000 companies worldwide, handling billions of mission-critical transactions every day. 
* Only vendor with out-of-the-box support solutions for Android, iOS and Windows in the same platform

Release Version:

The current release version of Entgra IoT server is 3.8.0. See the Release Archive for features specific to each of the past versions.

Related Links:

* Presentation on Entgra EMM 

Notes/References:

* *1 WSO2 - The Technology Partner for and the designated reseller of Entgra.
* *2 Essential EMM Suite capabilities as identified by Gartner’s Magic Quadrant for year 2019. These include MDM, MAM, MI (Mobile Identity), Mobile Content Management (MCM), Containment and EUM, all of which are supported by Entgra IoT server.
Magic Quadrant for Enterprise Mobility Management Suites 
Gartner Names 4 Leaders in Enterprise Mobility Management MQ 
* *3 Entgra is listed under Google Certified Enterprise Partners in 2019 (https://androidenterprisepartners.withgoogle.com/provider/#!/nO0FRKVachIRfVcd1gby).

