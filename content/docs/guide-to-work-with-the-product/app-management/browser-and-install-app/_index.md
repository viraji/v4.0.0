---
bookCollapseSection: true
weight: 4
---


# Installing and Uninstalling Applications

<br><iframe width="560" height="315" src="https://www.youtube.com/embed/AKVw3tyzJ1w" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<strong><h2>How to Install & Uninstall Applications</h2></strong>

The Entgra Agent should have been installed to the device before installing and uninstalling applications.

<h2>Steps</h2>

<ul style="list-style-type:decimal;">
    <li>Go to the Applications page in the Store portal.</li>
    <li>Find the install button section on the app page.</li>
    <li>Click the button and choose the section eg:- device,user,role & group.</li>
    <li>If it is a device section then choose the devices or if it is any other section please fill the input field.</li>
    <li>After that Installgit  the application.</li>
    <li>Do the same thing to the uninstall.</li>
</ul>

<strong><h2>Scheduled Install & Uninstall</h2></strong>

<h2>Steps</h2>

<ul style="list-style-type:decimal;">
    <li>Go to application page which is in the store portal.</li>
    <li>Find the install button section on the app page.</li>
    <li>Click the button and choose the section eg:- device,user,role & group.</li>
    <li>If it is a device section then choose the devices or if it is any other section please fill the input field.</li>
    <li>Go to time section and give the future time to install the app.</li>
    <li>After that Install the application.</li>
    <li>Do the same thing to the schedule uninstall.</li>
</ul>
