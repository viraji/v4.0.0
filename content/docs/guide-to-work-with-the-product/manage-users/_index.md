---
bookCollapseSection: true
weight: 8
---

# User Management

User management is a mechanism which involves defining and managing users, roles and their access levels in a system. A typical user management implementation involves a wide range of functionality such as adding/deleting users, controlling user activity through permissions, managing user roles, defining authentication policies, resetting user passwords etc.

Please note the following before you begin:

Your product has a primary user store where the users/roles that you create using the management console are stored by default. It's default RegEx configurations are as follows. RegEx configurations ensure that parameters like the length of a user name/password meet the requirements of the user store.


        PasswordJavaRegEx-------- ^[\S]{5,30}$
        PasswordJavaScriptRegEx-- ^[\S]{5,30}$
        UsernameJavaRegEx-------- ^~!#$;%*+={}\\{3,30}$
        UsernameJavaScriptRegEx-- ^[\S]{3,30}$
        RolenameJavaRegEx-------- ^~!#$;%*+={}\\{3,30}$
        RolenameJavaScriptRegEx-- ^[\S]{3,30}$


When creating users/roles, if you enter a username, password etc. that does not conform to the RegEx configurations, the system throws an exception. You can either change the RegEx configuration or enter values that conform to the RegEx. If you change the default user store, configure the RegEx accordingly under the user store manager configurations in <IoTS_HOME>/conf/user-mgt.xml file.

## Managing Users

Users are the people who interact with Entgra IoTS. Users can belong to either the administrator or user role type. The super tenant administrator and a tenant administrator belong to the administrator role type; while, all other end-users belong to the user role type. The organization does not need to add users manually, if Entgra IoTS is connected to a user store. IoTS users are not allow to edit user details once they have been added. You need to delete the current user and create a new user, if the need arises to change user details.


### Adding Users

Follow the instructions below to add a user:

1.  [Sign in to the Entgra IoT Server console]({{< param doclink >}}guide-to-work-with-the-product/login-guide/).


    If you want to try out Entgra IoT Server as an admin, use **admin** as the username and the password.


2.  You can navigate to the **ADD USER** page via the following methods: 

    1.  Method 01: 
        
        Click **menu** icon 
    
        <img src = "../../image/4000.png" style="border:5px solid black "> 
    
        Select **USER MANAGEMENT ** 
    
        <img src = "../../image/12345.png" style="border:5px solid black">
        
        Select **USERS**
        
        <img src = "../../image/600556.png" style="border:5px solid black">
        
        Select **ADD USER** button.  
        
        <img src = "../../image/600057.png" style="border:5px solid black">
        
    2.  Method 02: Click **Add** icon on the **USER** tile. 
    
        <img src = "../../image/11111.png" style="border:5px solid black"> 
    
3. Provide the required details and click **Add User**.
        *   **User Name**: Provide the user name. Should be in minimum 3 characters long and do 
                            not include any whitespaces.
        *   **First Name**: Provide the first name. 
        *   **Last Name**: Provide the last name.
        *   **Email Address**: Provide the email address. 
        *   **User Roles**: Optional field that can have 0-to-many roles for the user. 
        
        <img src = "../../image/600059.png" style="border:5px solid black">
        
### Viewing Users

1.  [Sign in to the Entgra IoT Server console]({{< param doclink >}}guide-to-work-with-the-product/login-guide/).

    If you want to try out Entgra IoT Server as an admin, use **admin** as the username and the password.

2.  View the Users

    a.Navigate via the menu:
    
       i.  Click the menu icon.
        
       <img src = "../../image/4000.png" style="border:5px solid black "> 

       ii. Select **USER MANAGEMENT ** 
        
       <img src = "../../image/12345.png" style="border:5px solid black">

       iii. Select **USERS**
        
    b.To navigate via the DASHBOARD page, click View under USERS to view all the users.

    <img src = "../../image/600061.png" style="border:5px solid black">

Then the list of users will be displayed.

<img src = "../../image/600060.png" style="border:5px solid black">

3.Click on the user you wish to view.

### Updating User Details

1.  [Sign in to the Entgra IoT Server console]({{< param doclink >}}guide-to-work-with-the-product/login-guide/).

    If you want to try out Entgra IoT Server as an admin, use **admin** as the username and the password.

2.  View the Users

    a.Navigate via the menu:
    
       i.  Click the menu icon.
        
       <img src = "../../image/4000.png" style="border:5px solid black "> 

       ii. Select **USER MANAGEMENT ** 
        
       <img src = "../../image/12345.png" style="border:5px solid black">

       iii. Select **USERS**
        
    b.To navigate via the DASHBOARD page, click View under USERS to view all the users.

    <img src = "../../image/600061.png" style="border:5px solid black">
    
3.  Click the edit icon on the user you wish to update.

    <img src = "../../image/600071.png" style="border:5px solid black">
    
4.  Do the required changes and click Save User.

    <img src = "../../image/600072.png" style="border:5px solid black">

### Resetting a User Password

<strong>Admin user, resetting other user's password</strong>

1.  [Sign in to the Entgra IoT Server console]({{< param doclink >}}guide-to-work-with-the-product/login-guide/).

    If you want to try out Entgra IoT Server as an admin, use **admin** as the username and the password.

2.  View the Users

    a.Navigate via the menu:
    
       i.  Click the menu icon.
        
       <img src = "../../image/4000.png" style="border:5px solid black "> 

       ii. Select **USER MANAGEMENT ** 
        
       <img src = "../../image/12345.png" style="border:5px solid black">

       iii. Select **USERS**
        
    b.To navigate via the DASHBOARD page, click View under USERS to view all the users.

    <img src = "../../image/600061.png" style="border:5px solid black">
    
3. Click the  icon on the user whose password you want to reset. 
 
    <img src = "../../image/600064.png" style="border:5px solid black">  
     
4.  A password entry dialog appears. Enter the new password and click Save. 
    
    <img src = "../../image/600065.png" style="border:5px solid black">  


<strong>Device owner changing their own password</strong>

1.  [Sign in to the Entgra IoT Server console]({{< param doclink >}}guide-to-work-with-the-product/login-guide/).

2.  Click the user icon, and click Change Password.

    <img src = "../../image/600066.png" style="border:5px solid black">
    
3.  Enter the required details and click Change.

    <img src = "../../image/600067.png" style="border:5px solid black">
    
4. There will be a pop up message stating your password is changed, and you will be logged out automattically. You will be able to login again to the server using your new password.


### Removing a User

1.  [Sign in to the Entgra IoT Server console]({{< param doclink >}}guide-to-work-with-the-product/login-guide/).

    If you want to try out Entgra IoT Server as an admin, use **admin** as the username and the password.

2.  View the Users

    a.Navigate via the menu:
    
       i.  Click the menu icon.
        
       <img src = "../../image/4000.png" style="border:5px solid black "> 

       ii. Select **USER MANAGEMENT ** 
        
       <img src = "../../image/12345.png" style="border:5px solid black">

       iii. Select **USERS**
        
    b.To navigate via the DASHBOARD page, click View under USERS to view all the users.

    <img src = "../../image/600061.png" style="border:5px solid black">
    
3.  Click the remove icon on the user you wish to remove.

    <img src = "../../image/600062.png" style="border:5px solid black">
    
4.  Click REMOVE to confirm that you want to remove the user.

    <img src = "../../image/600063.png" style="border:5px solid black"> 
    

### Inviting Users

1.  [Sign in to the Entgra IoT Server console]({{< param doclink >}}guide-to-work-with-the-product/login-guide/).

    If you want to try out Entgra IoT Server as an admin, use **admin** as the username and the password.

2.  View the Users

    a.Navigate via the menu:
    
       i.  Click the menu icon.
        
       <img src = "../../image/4000.png" style="border:5px solid black "> 

       ii. Select **USER MANAGEMENT ** 
        
       <img src = "../../image/12345.png" style="border:5px solid black">

       iii. Select **USERS**
        
    b.To navigate via the DASHBOARD page, click View under USERS to view all the users.

    <img src = "../../image/600061.png" style="border:5px solid black">

3.  Search for users you wish to invite, select them by clicking the respective users and click Invite Selected.
    Click Select, to select the users click on the users you wish invite, and click Invite Selected.
    
    <img src = "../../image/600069.png" style="border:5px solid black">
    
4.  Pop up message will be displayed stating an invitation mail will be sent to the selected user
    (s) to initiate an enrolment process. Click Yes to confirm.
    
    <img src = "../../image/600070.png" style="border:5px solid black">

### Searching, Filtering and Sorting Users

1.  [Sign in to the Entgra IoT Server console]({{< param doclink >}}guide-to-work-with-the-product/login-guide/).

    If you want to try out Entgra IoT Server as an admin, use **admin** as the username and the password.

2.  View the Users

    a.Navigate via the menu:
    
       i.  Click the menu icon.
        
       <img src = "../../image/4000.png" style="border:5px solid black "> 

       ii. Select **USER MANAGEMENT ** 
        
       <img src = "../../image/12345.png" style="border:5px solid black">

       iii. Select **USERS**
        
    b.To navigate via the DASHBOARD page, click View under USERS to view all the users.

    <img src = "../../image/600061.png" style="border:5px solid black">
    
3.  Filter the users based on the following criteria:<br>
        <br>By User Name - Define the user name of the user. The user name is defined using the 
        lower case with no white spaces when adding the user (example: fooToo).
        <br>By First Name - Define the first name of the user (example: Foo).
        <br>By Last Name - Define the last name of the user (example: Too).
        <br>By Email - Define the email of the user (example: foo@bar.com).
        
    <img src = "../../image/600073.png" style="border:5px solid black">


## Using Email Address as the Username

Entgra products can be configured to authenticate users using their attributes. For example, you can use attributes such as email or mobile number to log in instead of logging in using the username. This topic provides instructions on how to set up Entgra IoT Server to authenticate users using their email address. 

1.  Open the <IOTS_HOME>/conf/carbon.xml file.
2.  Find and uncomment the EnableEmailUserName configuration to enable email authentication.

```
<EnableEmailUserName>true</EnableEmailUserName>
```

3.  Open the <IOTS_HOME>/conf/identity/identity-mgt.properties file and set the following property to true to enable hashed usernames.


   This step is required due to a known issue that prevents the confirmation codes from being removed after they are used, when email usernames are enabled. This occurs because the '@' character and some special characters are not allowed in the registry. To overcome this issue, enable hashed usernames when saving the confirmation codes by configuring the properties below.


```
UserInfoRecovery.UseHashedUserNames=true
```

Optionally, configure the following property to determine which hash algorithm to use.

```
UserInfoRecovery.UsernameHashAlg=SHA-1
```

4.Open the <IOTS_HOME>/conf/user-mgt.xml file and add the following property under the relevant 
user store manager tag. This property specifies the username validation that will be enforced when the EnableEmailUserName option is enabled, using regex (regular expression).

```

Tip: Theuser-mgt.xmlfile consists of configurations for the primary user store. To configure this for a secondary user store, modify the relevant user store configuration file found in the  <IOTS_HOME>/deployment/server/userstoresdirectory instead.
``` 

<strong>Sample regex</strong>
```
<Property name="UsernameWithEmailJavaScriptRegEx">^[\S]{3,30}$</Property>
``` 

5.Configure the following parameters in the user-mgt.xml file under the relevant user store manager tag, depending on the type of user store you are connected to (LDAP/Active Directory/ JDBC).

<strong>JDBC User Store</strong>

Note the following when configuring the user store manager:
<ul style="list-style-type:disc;">
    <li>The properties listed below are not available by default for the JDBCUserStoreManager. Therefore, if you are using a JDBC-based user store, you only need to replace the following property value under the user store tag. This property allows you to add special characters such as "@" in the username. ```
        <Property name="UsernameJavaScriptRegEx">^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$</Property>
        ``` For any other type of user store, configure the properties listed below accordingly.
    </li>
    <li>If the UsernameWithEmailJavaScriptRegEx property has a regular expression including the "@" symbol, it is not necessary to configure the UsernameJavaRegEx and UsernameJavaScriptRegEx properties. The priority order to configure username regular expression properties, are as follows: a. UsernameJavaRegEx b. UsernameWithEmailJavaScriptRegEx
    </li>
</ul>

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
    <tr>
                <th>Parameter</th>
                <th>Description</th>
        </tr>
        <tr>
            <td>UserNameAttribute</td>
            <td>Set the mail attribute of the user.
                <br> &lt;Property name="UserNameAttribute">mail&lt;/Property></td>
        </tr>
        <tr>
            <td>UserNameSearchFilter</td>
            <td>Use the mail attribute of the user instead of cn or uid.
                <br> &lt;
                <Property name="UserNameSearchFilter">(&amp;(objectClass=identityPerson)(mail=?))&lt;/Property></td>
        </tr>
        <tr>
            <td>UserNameListFilter</td>
            <td>Use the mail attribute of the user.
                <br> &lt;Property name="UserNameListFilter">(&amp;(objectClass=identityPerson)(mail=*))l&lt;/Property></td>
        </tr>
        <tr>
            <td>UserDNPattern</td>
            <td>This parameter is used to speed up the LDAP search operations. You can comment out this config.
                <br> &lt;!--Property name="UserDNPattern">cn={0},ou=Users,dc=wso2, dc=com&lt;/Property-->
            </td>
        </tr>
        <tr>
            <td>UsernameJavaScriptRegEx</td>
            <td>Change this property under the relevant user store manager tag as follows. This property allows you to add special characters like "@" in the username.
                <br> &lt;Property name="UsernameJavaScriptRegEx">^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$&lt;/Property></td>
        </tr>
        <tr>
            <td>UsernameJavaRegEx</td>
            <td>This is a regular expression to validate usernames. By default, strings have a length of 5 to 30. Only non-empty characters are allowed. You can provide ranges of alphabets, numbers and also ranges of ASCII values in the RegEx properties.
                <br> &lt;Property name="UsernameJavaRegEx">a-zA-Z0-9@._-{3,30}$&lt;/Property></td>
        </tr>
        <tr>
            <td>Realm configurations</td>
            <td>The AdminUser username must use the email attribute of the admin user.
                <br> &lt;AdminUser>
                <br> &lt;UserName>admin@wso2.com&lt;/UserName>
                <br> &lt;Password>admin&lt;/Password>
                <br> &lt;/AdminUser>
            </td>
        </tr>
    </tbody>
</table>


To try out this scenario, create a new user with email address as the username and attempt to login to the Entgra IoT Server device management application using the new user's credentials. 

1.  [Sign in to the Entgra IoT Server console]({{< param doclink >}}guide-to-work-with-the-product/login-guide/).

If you want to try out Entgra IoT Server as an admin, use **admin** as the username and the password.

2.  You can navigate to the **ADD USER** page via the following methods: 

    1.  Method 01: 
        
        Click **menu** icon 
    
        <img src = "../../image/4000.png" style="border:5px solid black "> 
    
        Select **USER MANAGEMENT ** 
    
        <img src = "../../image/12345.png" style="border:5px solid black">
        
        Select **USERS**
        
        <img src = "../../image/600556.png" style="border:5px solid black">
        
        Select **ADD USER** button.  
        
        <img src = "../../image/600057.png" style="border:5px solid black">
        
    2.  Method 02: Click **Add** icon on the **USER** tile. 
    

        <img src = "../../image/11111.png" style="border:5px solid black"> 


 3.  Use an email address as the username when creating the new user, as seen below.
    
    <img src = "../../image/600074.png" style="border:5px solid black"> 
    
    Click **Add User**
    
 4.Login to the management console via https://localhost:9443/carbon. 
     
 5.Click on List under Users and Roles on the Main tab, and then click Users. 
 
 6.The new user you created will be listed. Click Change Password to set a new password for the 
 user. 
 
 <img src = "../../image/600075.png" style="border:5px solid black">
 
 7.Access the device management console again and login using the new user's credentials:
 
        Username: kimentgra@gmail.com
        Password: kimwso2123
        
   You have successfully configured using email address as the username, and logged in using an email address. 
    
