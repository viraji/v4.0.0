---
bookCollapseSection: true
weight: 3
---

# Login Guide

Device management portal app in the management related web application that EMM admins may use to manage devices. To login to this portal, 


<ul style="list-style-type:disc;">
    <li>Visit the devicemgt app using your browser. This can be accessed with the following url format, https://Replace-Your-IP-Address:9443/devicemgt For example https://localhost:9443/devicemgt
    </li>
    <li>Then you will get a security exception like bellow, since SSL certificates are not setup. Click "Advanced" then "Proceed to localhost (unsafe)"</li>
    <img src="../../image/60001.png" style="border:5px solid black">
    <li>Then browser will be redirected to the login page. Type the default username "admin" and the password "admin" and click login.</li>
    <img src="../../image/60002.png" style="border:5px solid black">
    <li>Finally browser will be redirected to the device management portal which looks like bellow .
    </li>
    <img src="../../image/60003.png" style="border:5px solid black">
</ul>