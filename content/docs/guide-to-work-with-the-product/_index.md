---
bookCollapseSection: true
weight: 2
---

# Working with the Product

The Product Guide section is aimed at guiding you with step-by-step procedures and videos on working with the Entgra IoT server. Covered within this, is first the System Requirements outlining the required specifications for running Entgra IoT in your system.

Downloading and starting the Entgra IoT server is covered next, followed by the  Log-in Guide section which illustrates the correct way to log in to the server.

Complete guides on enrolling Android, iOs, Mac OS and Windows devices are covered in the Enrollment Guide section. These include procedures for all types of enrollment available for iOS devices including the standard DEP enrollment, BYOD along with additional server configurations for each.  For Android devices, detailed procedures with and without using the QR code, including that of using the Entgra Agent are covered within this section. Additional Server configurations for Windows enrollments have been provided along with steps for enrolling Windows devices.

The Device Management Guide section takes you through instructions on how to manage an enrolled device, the required general platform configurations and detailed coverage on how operations and policies are applicable to each Android and Apple device type. 

Managing a multi-tenant system whilst achieving maximum resource sharing across multiple users ensuring optimal performance is explained in detail under the Manage Tenant section. 

A segment on how to assign and manage Roles and permissions is given under Manage Roles. This is followed up by the user management section covered under Manage Users. 

The App Management section takes you through on how to publish a new app, its releases and lifecycle management. This also covers Google Enterprise App Management  and also steps on how to Install and Uninstall Apps. 

The Key Concepts section aims to take you through the main concepts used within and applied to in Mobile Device Management. Brief introductions to the concepts and terminology used will enable easier understanding of the applicable use cases. 
