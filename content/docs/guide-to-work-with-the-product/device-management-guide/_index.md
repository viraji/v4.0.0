---
bookCollapseSection: true
weight: 5
---

# Guide to Managing Devices

In managing the devices enrolled with the Entgra IoT server, it is important to set the general platform configurations to the frequency that the server needs to be monitored, and for deploying geo-analytics. Instructions for general platform configurations are given here.

Managing devices section guides you on how to group enrolled devices, share the groups specific to user roles, and to update and remove groups when required. This section also takes you through on how to monitor the device status. 

Also covered within this section are the Policies for Android and Apple devices. Procedures on How to Add, Update, Publish/Unpublish and View a policy enforced on a device are given. It is also possible to manage the priority order of the policies on a device.

Device operations that can be implemented on Android and Apple devices via the Entgra IoT server have been explained separately for each device type. Similarly, device policies that can be enforced on each device type are shown under Android Device Policies and Apple Device Policies.

Device Management documentation available here guides you completely on how to effectively manage your device with the Entgra IoT server. 
