---
bookCollapseSection: true
weight: 9
---

# COSU Profile Configuration

{{< hint info >}}
<b> <a href ="{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an Android device.
{{< /hint >}}

This policy can be used to configure the profile of COSU Devices


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Restrict Device Operation Time<br><i>Device will be operable only during the below time period.</i></strong></center>
            </td>
        </tr>
        <tr>
            <td><strong>Start Time</strong></td>
            <td>Start time for the device</td>
        </tr>
        <tr>
            <td><strong>End Time</strong></td>
            <td>Lock down time for the device</td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Device Global 
            Configuration</strong>
                    <br>Theme can be configured with the following options.
                </center>
            </td>
        </tr>
        <tr>
            <td><strong>Launcher background image</strong></td>
            <td>This is the image that will be displayed in kiosk background.
                <br>[ Should be a valid URL of jpg or jpeg or png ]</td>
        </tr>
        <tr>
            <td><strong>Company logo to display</strong></td>
            <td>Company logo to display in the kiosk app drower.
                <br>[ Should be a valid URL ending with .jpg, .png, .jpeg ]</td>
        </tr>
        <tr>
            <td><strong>Company name</strong></td>
            <td>Name of the company that have to appear on the agent.</td>
        </tr>
    </tbody>
</table>

<strong> Is single application mode </strong> This configuration allows user to enroll single application on Kiosk mode task.If user select more apps it will get the top most application.
<table>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Is single application mode</strong></td>
            <td>Selected initial app in <strong>Enrollment Application Install policy 
                config</strong> will be selected for single application mode. Atleast one application should be selected. If more than one application is beeing selected, then first selected application in the list will be installed as the single application mode.</td>
        </tr>
        <tr>
            <td><strong>Is application built for Kiosk</strong></td>
            <td>Is single mode app built for Kiosk. Enable if lock task method is called in the application</td>
        </tr>
    </tbody>
</table>

<img src="single_application.gif" style="border:5px solid black" alt="single_application_gif">

<strong> Is idle media enabled </strong> This Configuration allows user to display idle timeout video on the device.
<table>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Media to display while idle</strong></td>
            <td>Url of the media to display while the device is idle.[ Should be a valid URL ending with .jpg, .png, .jpeg, .mp4, .3gp, .wmv, .mkv ]</td>
        </tr>
        <tr>
            <td><strong>Idle graphic begin after(seconds)</strong></td>
            <td>Idle graphic begin after the defined seconds[ Idle timeout should be defined in seconds ]</td>
        </tr>
    </tbody>
</table>

<img src="media_enable.gif" style="border:5px solid black;width: 68%" alt="media_enable_gif">
<img src="media_device.gif" style="border:5px solid black;width: 21%" alt="media_device_gif">

<strong>Is multi-user device</strong>
<br>If Is multi-user device enabled, multi-user configuration can be done for one device. Which enables to register already installed applications for registered users. After the policy is applied these applications can only be executed by logging in as the registered user. Other than this common applications which are common to all the users also can be specified by this policy.
<table style="width: 100%;">
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Is login needed for user switch</strong></td>
            <td>If this is enabled, the user should have valid user name and password to login to the device.</td>
        </tr>
        <tr>
            <td><strong>Primary User Apps</strong></td>
            <td>Primary User is the user to which the device is enrolled. The applications that are specified in here will be available by default. These applications can be used by any user. Provide comma separated package name or web clip details for applications. eg: com.google.android.apps.maps, {"identity":"http:entgra.io/","title":"entgra-webclip"}</td>
        </tr>
    </tbody>
</table>
<img src="multi-user.gif" style="border:5px solid black; max-width:63%;" alt="multi user policy gif">
<img src="multi-user-mobile.gif" style="border:5px solid black; max-width:21%" alt="multi user policy gif">

<strong> Device display orientation </strong>

<table>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Device display orientation</strong></td>
            <td>The display orientation of device can be set in a fixed mode.
                <br>
                <ul>
                    <li>Auto</li>
                    <li>Potrait</li>
                    <li>Landscape</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>

<strong>Enable Browser Properties</strong>
<br>This can be used to restrict properties of the web browser while using web views.
<table style="width: 100%;">
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Primary URL</strong></td>
            <td>Primary URL of the web view</td>
        </tr>
        <tr>
            <td><strong>Enable top control bar</strong></td>
            <td>Enables top control bar that displays all the controllers such as address bar, home button and forwards controllers</td>
        </tr>
        <tr>
            <td><strong>Enable Browser Address Bar</strong></td>
            <td>Enables address bar of the browser.</td>
        </tr>
        <tr>
            <td><strong>Is allow to go back on a page</strong></td>
            <td>Enables to go back in a page</td>
        </tr>
        <tr>
            <td><strong>Is allow to go forward on a page</strong></td>
            <td>Enables to go forward in a page</td>
        </tr>
        <tr>
            <td><strong>Is home button enabled</strong></td>
            <td>Enables browser's home button</td>
        </tr>
        <tr>
            <td><strong>Is page reload enabled</strong></td>
            <td>Enables page reload</td>
        </tr>
        <tr>
            <td><strong>Only allowed to visit the primary url</strong></td>
            <td>Enables visiting URLs other than the primary url</td>
        </tr>
        <tr>
            <td><strong>Is javascript enabled</strong></td>
            <td>Enables loading of javascript from the browser</td>
        </tr>
        <tr>
            <td><strong>Is copying text from browser enabled</strong></td>
            <td>Enables copying texts in the browser</td>
        </tr>
        <tr>
            <td><strong>Is downloading files enabled</strong></td>
            <td>Enable downloading files from the browser</td>
        </tr>
        <tr>
            <td><strong>Is Kiosk limited to one webapp</strong></td>
            <td>Sets whether device can access single or multiple web views</td>
        </tr>
        <tr>
            <td><strong>Is form auto-fill enabled</strong></td>
            <td>Enables autofill to forms in the browser</td>
        </tr>
        <tr>
            <td><strong>Is content access enabled</strong></td>
            <td>Enables content URL access within WebView. Content URL access allows WebView to load content from a content provider installed in the system.</td>
        </tr>
        <tr>
            <td><strong>Is file access enabled</strong></td>
            <td>Sets whether JavaScript running in the context of a file scheme URL should be allowed to access content from other file scheme URLs. .</td>
        </tr>
        <tr>
            <td><strong>Is allowed universal access from file URLs</strong></td>
            <td>Sets whether JavaScript running in the context of a file scheme URL should be allowed to access content from any origin.</td>
        </tr>
        <tr>
            <td><strong>Is application cache enabled.</strong></td>
            <td>Enables web view's application cache</td>
        </tr>
        <tr>
            <td><strong>Application cache file path</strong></td>
            <td>Sets the path to the Application Caches files. In order for the Application Caches API to be enabled, this method must be called with a path to which the application can write</td>
        </tr>
        <tr>
            <td><strong>Application cache mode</strong></td>
            <td>Overrides the way the cache is used. The way the cache is used is based on the navigation type. For a normal page load, the cache is checked and content is re-validated as needed. When navigating back, content is not re-validated, instead the content is just retrieved from the cache. This method allows the client to override this behavior by specifying one of LOAD_DEFAULT, LOAD_CACHE_ELSE_NETWORK, LOAD_NO_CACHE or LOAD_CACHE_ONLY</td>
        </tr>
        <tr>
            <td><strong>Should load images</strong></td>
            <td>Sets whether the browser should load image resources(through network and cached). Note that this method controls loading of all images, including those embedded using the data URI scheme.</td>
        </tr>
        <tr>
            <td><strong>Block image loads via network</strong></td>
            <td>Sets whether the browser should not load image resources from the network (resources accessed via http and https URI schemes)</td>
        </tr>
        <tr>
            <td><strong>Block all resource loads from network</strong></td>
            <td>title="Sets whether the browser should not load any resources from the network."</td>
        </tr>
        <tr>
            <td><strong>Support zooming</strong></td>
            <td>Sets whether the browser should support zooming using its on-screen zoom controls and gestures.</td>
        </tr>
        <tr>
            <td><strong>Show on-screen zoom controllers</strong></td>
            <td>Sets whether the browser should display on-screen zoom controls. Gesture based controllers are still available</td>
        </tr>
        <tr>
            <td><strong>Text zoom percentage</strong></td>
            <td>Sets the text zoom of the page in percent(Should be a positive number)</td>
        </tr>
        <tr>
            <td><strong>Default font size</strong></td>
            <td>Sets the default font size of the browser(Should be a positive number between 1 and 72)</td>
        </tr>
        <tr>
            <td><strong>Default text encoding name</strong></td>
            <td>Sets the default text encoding name to use when decoding html pages(Should a valid text encoding)</td>
        </tr>
        <tr>
            <td><strong>Is database storage API enabled</strong></td>
            <td>Sets whether the database storage API is enabled.</td>
        </tr>
        <tr>
            <td><strong>Is DOM storage API enabled</strong></td>
            <td>Sets whether the DOM storage API is enabled</td>
        </tr>
        <tr>
            <td><strong>Is Geolocation enabled</strong></td>
            <td>Sets whether Geolocation API is enabled</td>
        </tr>
        <tr>
            <td><strong>Can JavaScript open windows</strong></td>
            <td>JavaScript can open windows automatically or not. This applies to the JavaScript function window.open()</td>
        </tr>
        <tr>
            <td><strong>Does media playback requires user consent</strong></td>
            <td>Sets whether the browser requires a user gesture to play media. If false, the browser can play media without user consent</td>
        </tr>
        <tr>
            <td><strong>Is safe browsing enabled</strong></td>
            <td>Sets whether safe browsing is enabled. Safe browsing allows browser to protect against malware and phishing attacks by verifying the links</td>
        </tr>
        <tr>
            <td><strong>Use wide view port</strong></td>
            <td>Sets whether the browser should enable support for the viewport HTML meta tag or should use a wide viewport. When the value of the setting is false, the layout width is always set to the width of the browser control in device-independent (CSS) pixels. When the value is true and the page contains the viewport meta tag, the value of the width specified in the tag is used. If the page does not contain the tag or does not provide a width, then a wide viewport will be used</td>
        </tr>
        <tr>
            <td><strong>Browser user agent string</strong></td>
            <td>Sets the WebView's user-agent string(Should be a valid user agent string)</td>
        </tr>
        <tr>
            <td><strong>Mixed content mode</strong></td>
            <td>Configures the browser's behavior when a secure origin attempts to load a resource from an insecure origin.</td>
        </tr>
    </tbody>
</table>
<img src="browser-restriction.gif" style="border:5px solid black; max-width:63%" alt="browser restriction policy gif">

{{< hint info >}}
<b> <a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/android-device/android-device
-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}
