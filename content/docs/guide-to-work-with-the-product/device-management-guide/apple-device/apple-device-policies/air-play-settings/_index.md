---
bookCollapseSection: true
weight: 6
---

# AirPlay Settings 

{{< hint info >}}
<b> <a href ="{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an iOS device.
{{< /hint >}}

This configuration can be used to define settings for connecting to AirPlay destinations. 
Once this configuration profile is installed on an iOS device, corresponding users will not be able 
to modify these settings on their devices.


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>AirPlay Credentials</strong>
                    <br>(If present, sets passwords for known AirPlay destinations.)</center>
            </td>
        </tr>
        <tr>
            <td><strong>Device Name</strong></td>
            <td>The name of the AirPlay destination (used on iOS).
            </td>
        </tr>
        <tr>
            <td><strong>Password</strong></td>
            <td>The password for the AirPlay destination.</td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>AirPlay Destinations Whitelist</strong>
                    <br>(Supervised only. If present, only AirPlay destinations present in this list are available to the device.)</center>
            </td>
        </tr>
        <tr>
            <td><strong>Destination</strong></td>
            <td>The Device ID of the AirPlay destination, in the format xx:xx:xx:xx:xx:xx. This field is not case sensitive.
            </td>
        </tr>
    </tbody>
</table>

{{< hint info >}}
<b> <a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}
