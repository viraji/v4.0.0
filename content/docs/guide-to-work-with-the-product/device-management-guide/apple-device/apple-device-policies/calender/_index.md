---
bookCollapseSection: true
weight: 10
---

# Calendar

{{< hint info >}}
<b> <a href ="{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an iOS device.
{{< /hint >}}

This configuration can be used to define settings for connecting to CalDAV servers. Once this configuration profile is installed on an iOS device, corresponding users will not be able to modify these settings on their devices.

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
        </tr>
        <tr>
            <td><strong>Account Description</strong></td>
            <td>Display name of the account. Eg: Company CalDAV Account
            </td>
        </tr>
        <tr>
            <td><strong>Account Hostname</strong></td>
            <td>CalDAV Host name or IP address</td>
        </tr>
        <tr>
            <td><strong>Use Secure Socket Layer(SSL)</strong></td>
            <td>Having this checked, would enable Secure Socket Layer communication with CalDAV server.
            </td>
        </tr>
        <tr>
            <td><strong>Account Port</strong></td>
            <td>CalDAV account Host Port number</td>
        </tr>
        <tr>
            <td><strong>Principal URL</strong></td>
            <td>Principal URL for the CalDAV account</td>
        </tr>
        <tr>
            <td><strong>Account Username</strong></td>
            <td>CalDAV account user name</td>
        </tr>
        <tr>
            <td><strong>Account Password</strong></td>
            <td>CalDAV account password
            </td>
        </tr>
    </tbody>
</table>

{{< hint info >}}
<b> <a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}
