---
bookCollapseSection: true
weight: 2
---

# Restrictions


{{< hint info >}}
<b> <a href ="{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an iOS device.
{{< /hint >}}

These configurations can be used to restrict apps, device features and media content available on an iOS device. Once this configuration profile is installed on a device, corresponding users will not be able to modify these settings on their devices.

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td colspan="2"><strong><center>Restrictions on mac OS and iOS device</center></strong></td>
        </tr>
        <tr>
            <td><strong>Allow Siri</strong></td>
            <td>When false, disables Siri. Defaults to true.</td>
        </tr>
        <tr>
            <td><strong>Allow use of camera</strong></td>
            <td>Having this checked would enable Usage of phone camera in the device
            </td>
        </tr>
        <tr>
            <td><strong>Allow iCloud documents and data</strong>
                <br> <i>[This key is deprecated 
            on unsupervised devices.]</i></td>
            <td>Having this checked would enable syncing iCloud documents and data in the device. This is deprecated on unsupervised devices
                <br>Available in iOS 5.0 and later and in macOS 10.11and later.</td>
        </tr>
        <tr>
            <td><strong>Allow iCloud keychain</strong></td>
            <td> When false, disables iCloud keychain synchronization. Default is true.
                <br>Available in iOS 7.0 and later and macOS 10 .12 and later.</td>
        </tr>
        <tr>
            <td><strong>Allow fingerprint for unlock</strong></td>
            <td>If false, prevents Touch ID from unlocking a device.
                <br>Available in iOS 7 and later and in macOS 10.12.4 and later.</td>
        </tr>
        <tr>
            <td><strong>Allow in-app purchase</strong></td>
            <td>Having this checked would allow in-app purchase in the device.</td>
        </tr>
        <tr>
            <td><strong>Allow screenshots</strong></td>
            <td>If set to false, users canʼt save a screenshot of the display and are prevented from capturing a screen recording; it also prevents the Classroom app from observing remote screens.
        </tr>
        <tr>
            <td><strong>Enable AutoFill</strong></td>
            <td>When false, Safari auto-fill is disabled. Defaults to true.</td>
        </tr>
        <tr>
            <td><strong>Allow voice dialing while device is locked</strong></td>
            <td>When false, disables voice dialing if the device is locked with a passcode. Default is true.
            </td>
        </tr>
        <tr>
            <td><strong>Force encrypting all backups</strong></td>
            <td>Having this checked would force encrypting all backups.</td>
        </tr>
        <tr>
            <td><strong>Allow managed apps to store data in iCloud</strong></td>
            <td>If set to false, prevents managed applications from using iCloud sync.</td>
        </tr>
        <tr>
            <td><strong>Allow Activity Continuation</strong></td>
            <td>If set to false, Activity Continuation will be disabled. Defaults to true.
            </td>
        </tr>
        <tr>
            <td><strong>Allow backup of enterprise books</strong></td>
            <td> If set to false, Enterprise books will not be backed up. Defaults to true</td>
        </tr>
        <tr>
            <td><strong>Allow enterprise books data sync</strong></td>
            <td>If set to false, Enterprise books notes and highlights will not be synced. Defaults to true.</td>
        </tr>
        <tr>
            <td><strong>Allow cloud photo library</strong></td>
            <td>If set to false, disables iCloud Photo Library. Any photos not fully downloaded from iCloud Photo Library to the device will be removed from local storage.
            </td>
        </tr>
        <tr>
            <td><strong>Allow remote screen observation</strong></td>
            <td> If set to false, remote screen observation by the Classroom app is disabled. Defaults to true. This key should be nested beneath allowScreenShot as a sub-restriction. If allowScreenShot is set to false, it also
                <br>Available in iOS 9.3 and macOS 10.14.4 and later.
            </td>
        </tr>
        <tr>
            <td><strong>Allow adding Game Center friends</strong>
                <br>[This key is deprecated on unsupervised devices.]</td>
            <td>When false, prohibits adding friends to Game Center. This key is deprecated on unsupervised devices.</td>
        </tr>
        <tr>
            <td><strong>Allow Siri to query user-generated content from web</strong></td>
            <td> Supervised only. When false, prevents Siri from querying user-generated content from the web.
                <br>Available in iOS 7 and later.
            </td>
        </tr>
        <tr>
            <td><strong>Allow video conferencing </strong>
                <br>[This key is deprecated on unsupervised devices.]</td>
            <td>When false, disables video conferencing. This key is deprecated on unsupervised devices
            </td>
        </tr>
        <tr>
            <td><strong>Allow Safari</strong>
                <br>[This key is deprecated on unsupervised devices.]</td>
            <td>When false, the Safari web browser application is disabled and its icon removed from the Home screen. This also prevents users from opening web clips. This key is deprecated on unsupervised devices.
            </td>
        </tr>
        <tr>
            <td><strong>Allow multiplayer gaming</strong>
                <br>[This key is deprecated on unsupervised devices.]</td>
            <td>When false, prohibits multiplayer gaming. This key is deprecated on unsupervised devices.
            </td>
        </tr>
        <tr>
            <td><strong>Allow use of iTunes Store</strong></td>
            <td>When false, the iTunes Music Store is disabled and its icon is removed from the Home screen. Users cannot preview, purchase, or download content. This key is deprecated on unsupervised devices.</td>
        </tr>
        <tr>
            <td colspan="2"><strong><center>Following are DEP(Supervised) 
            only</center></strong></td>
        </tr>
        <tr>
            <td><strong>Force Delayed Software Updates</strong></td>
            <td> If set to true, delays user visibility of Software Updates. Defaults to false. On macOS, seed build updates will be allowed, without delay.
                <br>Available in iOS 11.3 and macOS 10.13</td>
        </tr>
        <tr>
            <td><strong>Allow Erase All Content And Settings</strong></td>
            <td> If set to false, disables the “Erase All Content And Settings” option in the Reset UI.</td>
        </tr>
        <tr>
            <td><strong>Allow Spotlight Internet results</strong></td>
            <td> If set to false, Spotlight will not return Internet search results.
                <br>Available in iOS and in macOS 10.11 and later.</td>
        </tr>
        <tr>
            <td><strong>Enforced Software Update Delay</strong></td>
            <td>This restriction allows the admin to set how many days a software update on the device will be delayed. With this restriction in place, the user will not see a software update until the specified number of days after the software update release date. The max is 90 days and the default value is 30.
                <br> Available in iOS 11.3 and macOS 10.13.4</td>
        </tr>
        <tr>
            <td><strong>Force Classroom Automatically Join Classes</strong></td>
            <td>If set to true, automatically give permission to the teacherʼs requests without prompting the student. Defaults to false
                <br>Available only in iOS 11.0 and macOS 10 .14.4 and later</td>
        </tr>
        <tr>
            <td><strong>Force Classroom Request Permission To Leave Classes</strong></td>
            <td> If set to true, a student enrolled in an unmanaged course via Classroom will request permission from the teacher when attempting to leave the course. Defaults to false.
                <br> Available only in iOS 11.3 and macOS 10.14.4 and later.</td>
        </tr>
        <tr>
            <td><strong>Force Classroom Unprompted App And Device Lock</strong></td>
            <td> If set to true, allow the teacher to lock apps or the device without prompting the student. Defaults to false
                <br> Available only in iOS 11.0 and macOS 10.14.4 and later.
            </td>
        </tr>
        <tr>
            <td><strong>Force Classroom Unprompted Screen Observation</strong></td>
            <td> If set to true, and ScreenObservationPermissionModificationAllowed is also true in the Education payload, a student enrolled in a managed course via the Classroom app will automatically give permission to that courseʼs teacherʼs requests to observe the studentʼs screen without prompting the student. Defaults to false.
                <br> Available only in iOS 11.0 and macOS 10.14.4 and later.</td>
        </tr>
        <tr>
            <td><strong>Allow Password Auto Fill</strong></td>
            <td>If set to false, users will not be able to use the AutoFill Passwords feature on iOS and will not be prompted to use a saved password in Safari or in apps. If set to false, Automatic Strong Passwords will also be disabled and strong passwords will not be suggested to users. Defaults to true.
                <br>Available only in iOS 12.0 and macOS 10 .14 and later.</td>
        </tr>
        <tr>
            <td><strong>Allow Password Proximity Requests</strong></td>
            <td>If set to false, a userʼs device will not request passwords from nearby devices. Defaults to true.
                <br>Available only in iOS 12.0 and macOS 10.14</td>
        </tr>
        <tr>
            <td><strong>Allow Password Sharing</strong></td>
            <td>If set to false, users can not share their passwords with the Airdrop Passwords feature. Defaults to true.
                <br>Available only in iOS 12.0 and macOS 10.14 and later.</td>
        </tr>
        <tr>
            <td><strong>Allow definition lookup</strong></td>
            <td> If set to false, disables definition lookup. Defaults to true.
                <br> Available in iOS 8.1.3 and later and in macOS 10.11.2 and later</td>
        </tr>
        <tr>
            <td><strong>Allow music service</strong></td>
            <td>If set to false, Music service is disabled and Music app reverts to classic mode. Defaults to true.
                <br>Available in iOS 9.3 and later and macOS 10.12 and later</td>
        </tr>
        <tr>
            <td colspan="2"><strong><center>Restrictions on iOS device</center></strong></td>
        </tr>
        <tr>
            <td><strong>Allow Siri while device is locked</strong></td>
            <td>When false, the user is unable to use Siri when the device is locked. Defaults to true. This restriction is ignored if the device does not have a passcode set.
            </td>
        </tr>
        <tr>
            <td><strong>Allow removing apps</strong>
                <br>[This key is deprecated on unsupervised devices .]
            </td>
            <td> When false, disables removal of apps from iOS device. This key is deprecated on unsupervised devices.
            </td>
        </tr>
        <tr>
            <td><strong>Allow iCloud backup</strong></td>
            <td> When false, disables backing up the device to iCloud.
            </td>
        </tr>
        <tr>
            <td><strong>Allow diagnostic submission</strong></td>
            <td> When false, this prevents the device from automatically submitting diagnostic reports to Apple. Defaults to true.
                <br>Available only in iOS 6.0 and later.
            </td>
        </tr>
        <tr>
            <td><strong>Allow explicit content </strong>
                <br>[This key is deprecated on unsupervised devices .]
            </td>
            <td>When false, explicit music or video content purchased from the iTunes Store is hidden. Explicit content is marked as such by content providers, such as record labels, when sold through the iTunes Store. This key is deprecated on unsupervised devices.
                <br>Available in iOS and in tvOS 11.3 and later
            </td>
        </tr>
        <tr>
            <td><strong>Allow global background fetch when roaming</strong></td>
            <td>When false, disables global background fetch activity when an iOS phone is roaming.</td>
        </tr>
        <tr>
            <td><strong>Show Notifications Center in lock screen</strong></td>
            <td>If set to false, the Notifications history view on the lock screen is disabled and users canʼt view past notifications. Though, when the device is locked, the user will still be able to view notifications when they arrive.
                <br>Available only in iOS 7.0 and later.</td>
        </tr>
        <tr>
            <td><strong>Show Today view in lock screen</strong></td>
            <td>If set to false, the Today view in Notification Center on the lock screen is disabled.
                <br>Available only in iOS 7.0 and later.</td>
        </tr>
        <tr>
            <td><strong>Allow documents from managed sources in unmanaged destinations</strong></td>
            <td> If false, documents in managed apps and accounts only open in other managed apps and accounts. Default is true.
                <br>Available only in iOS 7.0 and later</td>
        </tr>
        <tr>
            <td><strong>Allow documents from unmanaged sources in managed destinations</strong></td>
            <td>If set to false, documents in unmanaged apps and accounts will only open in other unmanaged apps and accounts. Default is true.
                <br>Available only in iOS 7.0 and later.</td>
        </tr>
        <tr>
            <td><strong>Show Passbook notifications in lock screen</strong></td>
            <td>If set to false, Passbook notifications will not be shown on the lock screen.This will default to true.
                <br>Available in iOS 6.0 and later.</td>
        </tr>
        <tr>
            <td><strong>Allow Photo Stream</strong></td>
            <td>When false, disables Photo Stream.
                <br>Available in iOS 5.0 and later.</td>
        </tr>
        <tr>
            <td><strong>Force Fraud warning</strong></td>
            <td> When true, Safari fraud warning is enabled. Defaults to false
                <br>Available in iOS 4.0 and later.</td>
        </tr>
        <tr>
            <td><strong>Enable Javascript</strong></td>
            <td>When false, Safari will not execute JavaScript. Defaults to true.
                <br>Available in iOS 4.0 and later.</td>
        </tr>
        <tr>
            <td><strong>Enable Pop-ups</strong></td>
            <td>When false, Safari will not allow pop-up tabs. Defaults to true.
                <br>Available in iOS 4.0 and later.
            </td>
        </tr>
        <tr>
            <td><strong>Accept cookies</strong></td>
            <td>Determines conditions under which the device will accept cookies. The user facing settings changed in iOS 11, though the possible values remain the same:
                <br> • 0: Prevent Cross-Site Tracking and Block All Cookies are enabled and the user canʼt disable either setting.
                <br> • 1 or 1.5: Prevent Cross-Site Tracking is enabled and the user canʼt disable it. Block All Cookies is not enabled, though the user can enable it.
                <br> • 2: Prevent Cross-Site Tracking is enabled and Block All Cookies is not enabled. The user can toggle either setting. (Default)
                <br> These are the allowed values and settings in iOS 10 and earlier:
                <br> • 0: Never
                <br> • 1: Allow from current website only
                <br> • 1.5: Allow from websites visited (Available in iOS 8.0 and later); enter ’
                <real>1.5</real>’
                <br> • 2: Always (Default)
                <br> In iOS 10 and earlier, users can always pick an option that is more restrictive than the payload policy, but not a less restrictive policy. For example, with a payload value of 1.5, a user could switch to Never, but not Always Allow.</td>
        </tr>
        <tr>
            <td><strong>Allow Shared Photo Stream</strong></td>
            <td> If set to false, Shared Photo Stream will be disabled.This will default to true.
                <br>Available in iOS 6.0 and later.</td>
        </tr>
        <tr>
            <td><strong>Allow untrusted TLS prompt</strong></td>
            <td>When false, automatically rejects untrusted HTTPS certificates without prompting the user.
                <br>Available in iOS 5.0 and later.</td>
        </tr>
        <tr>
            <td><strong>Require iTunes store password for all purchases</strong></td>
            <td>When true, forces user to enter their iTunes password for each transaction
                <br>Available in iOS 5.0 and later.</td>
        </tr>
        <tr>
            <td><strong>Limit ad tracking</strong></td>
            <td>If true, limits ad tracking. Default is false
                <br>Available only in iOS 7.0 and later</td>
        </tr>
        <tr>
            <td><strong>Force a pairing password for Airplay outgoing requests</strong></td>
            <td>If set to true, forces all devices receiving AirPlay requests from this device to use a pairing password. Default is false.
                <br>Available only in iOS 7.1 and later.</td>
        </tr>
        <tr>
            <td><strong>Force air drop unmanaged</strong></td>
            <td>If set to true, causes AirDrop to be considered an unmanaged drop target. Defaults to false.
                <br>Available in iOS 9.0 and later.
            </td>
        </tr>
        <tr>
            <td><strong>Force watch wrist detection</strong></td>
            <td>If set to true, a paired Apple Watch will be forced to use Wrist Detection. Defaults to false.
                <br>Available in iOS 8.2 and later.</td>
        </tr>
        <tr>
            <td><strong>Allow over-the-air PKI updates</strong></td>
            <td>If false, over-the-air PKI updates are disabled. Setting this restriction to false does not disable CRL and OCSP checks. Default is true.
                <br>Available only in iOS 7.0 and later.
            </td>
        </tr>
        <tr>
            <td><strong>Ratings region</strong></td>
            <td>This 2-letter key is used by profile tools to display the proper ratings for given region. It is not recognized or reported by the client.
                <br> Possible values:
                <br> • au: Australia
                <br> • ca: Canada
                <br> • fr: France
                <br> • de: Germany
                <br> • ie: Ireland
                <br> • jp: Japan
                <br> • nz: New Zealand
                <br> • gb: United Kingdom
                <br> • us: United States
                <br> Available in iOS and tvOS 11.3 and later</td>
        </tr>
        <tr>
            <td><strong>Allow content ratings</strong>
                <br>Having this checked would allow to set the maximum allowed ratings</td>
            <td colspan="2">
                <table style="width: 100%;">
                    <tbody>
                        <tr>
                            <td>Allowed content ratings for movies</td>
                            <td> This value defines the maximum level of movie content that is allowed on the device. Possible values (with the US description of the rating level): • 1000: All • 500: NC-17 • 400: R • 300: PG-13 • 200: PG • 100: G • 0: None
                                <br>Available only in iOS and tvOS 11.3 and later</td>
                        </tr>
                        <tr>
                            <td>Allowed content ratings for TV shows</td>
                            <td>This value defines the maximum level of TV content that is allowed on the device. Possible values (with the US description of the rating level): • 1000: All • 600: TV-MA • 500: TV-14 • 400: TV-PG • 300: TV-G • 200: TV-Y7 • 100: TV-Y • 0: None
                                <br>Available only in iOS and tvOS 11.3 and later.</td>
                        </tr>
                        <tr>
                            <td>Allowed content ratings for apps</td>
                            <td> This value defines the maximum level of app content that is allowed on the device. Possible values (with the US description of the rating level): • 1000: All • 600: 17+ • 300: 12+ • 200: 9+ • 100: 4+ • 0: None
                                <br>Available only in iOS 5 and tvOS 11.3 and later.</td>
                    </tbody>
                </table>
            </td>
            </tr>
            <tr>
                <td><strong>Allow enterprise app trust</td>
                    <td>If set to false removes the Trust Enterprise Developer button in
                        Settings->General->Profiles & Device Management, preventing
                        apps from being provisioned by universal provisioning profiles.
                        This restriction applies to free developer accounts but it does not
                        apply to enterprise app developers who are trusted because their
                        apps were pushed via MDM, nor does it revoke previously
                        granted trust. Defaults to true.<br>Available in iOS 9.0 and later.</td>
                </tr>
                <tr>
                    <td><strong>Show Control Center in lock screen</strong></td>
                <td>If false, prevents Control Center from appearing on the Lock screen.
                    <br>Available in iOS 7 and later.</td>
            </tr>
            <tr>
                <td><strong>Read unmanaged apps from managed contact accounts.</strong></td>
                <td>If set to true, unmanaged apps can read from managed contacts accounts. Defaults to false. if allowOpenFromManagedToUnmanaged is true, this restriction has no effect. A payload that sets this to true must be installed via MDM.
                    <br>Available only in iOS 12.0 and later</td>
            </tr>
            <tr>
                <td><strong>Allow Siri while device is locked</strong></td>
                <td>When false, the user is unable to use Siri when the device is locked. Defaults to true. This restriction is ignored if the device does not have a passcode set.
                </td>
            </tr>
            <tr>
                <td><strong>Allow removing apps</strong>
                    <br>[This key is deprecated on unsupervised devices .]
                </td>
                <td> When false, disables removal of apps from iOS device. This key is deprecated on unsupervised devices.
                </td>
            </tr>
            <tr>
                <td><strong>Allow iCloud backup</strong></td>
                <td> When false, disables backing up the device to iCloud.
                </td>
            </tr>
            <tr>
                <td><strong>Allow diagnostic submission</strong></td>
                <td> When false, this prevents the device from automatically submitting diagnostic reports to Apple. Defaults to true.
                    <br>Available only in iOS 6.0 and later.
                </td>
            </tr>
            <tr>
                <td><strong>Allow explicit content </strong>
                    <br>[This key is deprecated on unsupervised devices .]
                </td>
                <td>When false, explicit music or video content purchased from the iTunes Store is hidden. Explicit content is marked as such by content providers, such as record labels, when sold through the iTunes Store. This key is deprecated on unsupervised devices.
                    <br>Available in iOS and in tvOS 11.3 and later
                </td>
            </tr>
            <tr>
                <td><strong>Allow global background fetch when roaming</strong></td>
                <td>When false, disables global background fetch activity when an iOS phone is roaming.</td>
            </tr>
            <tr>
                <td colspan="2"><strong><center>Following are DEP(Supervised) 
                    only</center></strong></td>
            </tr>
            <tr>
                <td><strong>Allow user prompted profile installation</strong></td>
                <td>If set to false, the user is prohibitedfrom installing configuration profiles and certificates interactively. This will default to true.
                    <br>Available in iOS 6.0 and later</td>
            </tr>
            <tr>
                <td><strong>Allow Chat</strong></td>
                <td>When false, disables the use of iMessage with supervised devices. If the device supports text messaging, the user can still send and receive text messages
                    <br>Available in iOS 6.0 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow Cellular Plan Modification</strong></td>
                <td>If set to false, users canʼt change any settings related to their cellular plan. Defaults to true
                    <br> Available in iOS 11.0 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow USB Restricted Mode</strong></td>
                <td>If set to false, device will always be able to connect to USB accessories while locked. Defaults to true.
                    <br> Available only in iOS 11.4.1 and later
                </td>
            </tr>
            <tr>
                <td><strong>Allow ESIM Modification</strong></td>
                <td>If set to false, the user may not remove or add a cellular plan to the eSIM on the device. Defaults to true
                    <br>Available only in iOS 12.1 and later.</td>
            </tr>
            <tr>
                <td><strong>Modify Personal Hotspot Modification</strong></td>
                <td>If set to false, the user may not modify the personal hotspot setting. Defaults to true.
                    <br>Available only in iOS 12.2 and later.</td>
            </tr>
            <tr>
                <td><strong>Automatically set Date and Time</strong></td>
                <td>If set to true, the Date & Time “Set Automatically” feature is turned on and canʼt be turned off by the user. Defaults to false.
                    <br><i> Note: The 
                    deviceʼs time zone will only be updated when the device can determine its 
                    location (cellular connection or wifi with location services enabled).</i>
                    <br>Available only in iOS 12.0 </td>
            </tr>
            <tr>
                <td><strong>Allow modifying account settings</strong></td>
                <td>If set to false, account modification is disabled.
                    <br>Available only in iOS 7.0 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow modifying cellular data app settings</strong></td>
                <td>f set to false, changes to cellular data usage for apps are disabled.
                    <br>: Available only in iOS 7.0 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow Siri to query user-generated content from web</strong></td>
                <td>When false, prevents Siri from querying user-generated content from the web.
                    <br>Available in iOS 7 and later.</td>
            </tr>
            <tr>
                <td><strong>Enable iBookStore</strong></td>
                <td>If set to false, Apple Books will be disabled. This will default to true.
                    <br>Available in iOS 6.0 and later.</td>
            </tr>
            <tr>
                <td><strong>Enable iBookStore Erotica</strong></td>
                <td>If set to false, the user will not be able to download media from Apple Books that has been tagged as erotica. This will default to true.
                    <br>Available in iOS and in tvOS 11.3 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow Find My Friends modification</strong></td>
                <td>If set to false, changes to Find My Friends are disabled.
                    <br>Available only in iOS 7.0 and later. </td>
            </tr>
            <tr>
                <td><strong>Allow use of Game Center</strong></td>
                <td>When false, Game Center is disabled and its icon is removed from the Home screen. Default is true.
                    <br>Available only in iOS 6.0 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow Host Pairing</strong></td>
                <td>If set to false, host pairing is disabled with the exception of the supervision host. If no supervision host certificate has been configured, all pairing is disabled. Host pairing lets the administrator control which devices an iOS 7 device can pair with.
                    <br>Available only in iOS 7.0 and later.
                </td>
            </tr>
            <tr>
                <td><strong>Allow Enable Restrictions option</strong></td>
                <td>If set to false, disables the ”Enable Restrictions” option in the Restrictions UI in Settings. Default is true. On iOS 12 or later, if set to false disables the ”Enable ScreenTime” option in the ScreenTime UI in Settings and disables ScreenTime if already enabled.
                    <br>Available in iOS 8.0 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow News</strong></td>
                <td> If set to false, disables News. Defaults to true
                    <br> Available in iOS 9 .0 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow use of Podcasts</strong></td>
                <td>If set to false, disables podcasts. Defaults to true.
                    <br>Available in iOS 8.0 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow keyboard auto-correction</strong></td>
                <td>If set to false, disables keyboard auto-correction. Defaults to true .
                    <br>Available in iOS 8.1.3 and later</td>
            </tr>
            <tr>
                <td><strong>Allow keyboard spell-check</strong></td>
                <td>If set to false, disables keyboard spell-check. Defaults to true.
                    <br>Available in iOS 8.1.3 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow UI app installation</strong></td>
                <td>When false, the App Store is disabled and its icon is removed from the Home screen. However, users may continue to use Host apps (iTunes, Configurator) to install or update their apps. Defaults to true. In iOS 10 and later, MDM commands can override this restriction.
                    <br>Available in iOS 9 .0 and later</td>
            </tr>
            <tr>
                <td><strong>Allow keyboard shortcuts</strong></td>
                <td>If set to false, keyboard shortcuts cannot be used. Defaults to true.
                    <br>Available in iOS 9.0 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow passcode modification</strong></td>
                <td>If set to false, prevents the device passcode from being added, changed, or removed. Defaults to true. This restriction is ignored by shared iPads .
                    <br> Available in iOS 9.0 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow device name modification</strong></td>
                <td>If set to false, prevents device name from being changed. Defaults to true.
                    <br> Available in iOS 9.0</td>
            </tr>
            <tr>
                <td><strong>Allow wallpaper modification</strong></td>
                <td> If set to false, prevents wallpaper from being changed. Defaults to true .
                    <br>Available in iOS 9.0 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow automatic app downloads</strong></td>
                <td>If set to false, prevents automatic downloading of apps purchased on other devices. Does not affect updates to existing apps. Defaults to true.
                    <br>Available in iOS 9.0 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow radio service</strong></td>
                <td>If set to false, Apple Music Radio is disabled. Defaults to true.
                    <br> Available in iOS 9.3 and later.</td>
            </tr>
            <tr>
                <td><strong>Blacklisted app bundle Ids(comma separated)</strong></td>
                <td>If present, prevents bundle IDs listed in the array from being shown or launchable. Include the value com.apple.webapp to blacklist all webclips.
                    <br>Available in iOS 9.3 and later.</td>
            </tr>
            <tr>
                <td><strong>Whitelisted app bundle Ids(comma separated)</strong></td>
                <td>If present, allows only bundle IDs listed in the array from being shown or launchable. Include the value com.apple.webapp to whitelist all webclips.
                    <br>Available in iOS 9.3 and later</td>
            </tr>
            <tr>
                <td><strong>Allow diagnostic bluetooth modification</strong></td>
                <td>If set to false, prevents modification of Bluetooth settings. Defaults to true.
                    <br>Available in iOS 10.0 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow dictation</strong></td>
                <td>If set to false, disallows dictation input. Defaults to true.
                    <br>Available only in iOS 10.3 and later</td>
            </tr>
            <tr>
                <td><strong>Force WiFi white listing (Warning, wrong configuration could break communication)</strong></td>
                <td>If set to true, the device can join Wi-Fi networks only if they were set up through a configuration profile. Defaults to false.
                    <br>Available only in iOS 10.3 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow air print</strong></td>
                <td>If set to false, disallow AirPrint. Defaults to true.
                    <br>Available in iOS 11.0 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow air print credentials storage</strong></td>
                <td>If set to false, disallows keychain storage of username and password for Airprint. Defaults to true.
                    <br> Available in iOS 11.0 and later.</td>
            </tr>
            <tr>
                <td><strong>Force air print trusted TLS requirement</strong></td>
                <td>If set to true, requires trusted certificates for TLS printing communication. Defaults to false.
                    <br> Available in iOS 11.0 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow air print iBeacon discovery</strong></td>
                <td>If set to false, disables iBeacon discovery of AirPrint printers. This prevents spurious AirPrint Bluetooth beacons from phishing for network traffic. Defaults to true.
                    <br>Available in iOS 11.0 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow system app removal</strong></td>
                <td>If set to false, disables the removal of system apps from the device. Defaults to true.
                    <br>Available only in iOS 11.0 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow VPN creation</strong></td>
                <td>If set to false, disallow the creation of VPN configurations. Defaults to true.
                    <br>Available only in iOS 11.0 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow proximity setup to new device</strong></td>
                <td>If set to false, disables the prompt to setup new devices that are nearby . Defaults to true.
                    <br>Available only in iOS 11.0 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow installing apps</strong></td>
                <td>When false, the App Store is disabled and its icon is removed from the Home screen. Users are unable to install or update their applications. This key is deprecated on unsupervised devices. MDM commands can override this restriction.
                    <br>Available only in iOS 10 and later</td>
            </tr>
            <tr>
                <td><strong>Allow AirDrop</strong></td>
                <td>If set to false, AirDrop is disabled.
                    <br>Available only in iOS 7.0 and later.
                </td>
            </tr>
            <tr>
                <td><strong>Permitted Applications in Autonomous Single App Mode</strong></td>
                <td>If present, allows apps identified by the bundle IDs listed in the array to autonomously enter Single App Mode.
                    <br>Available only in iOS 7.0 and later.
                    <br><strong>Application Bundle ID:</strong></td>
            </tr>
            <tr>
                <td><strong>Allow diagnostic submission modification</strong></td>
                <td>When false, this prevents the device from automatically submitting diagnostic reports to Apple. Defaults to true.
                    <br>Available only in iOS 6.0 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow diagnostic submission modification</strong></td>
                <td>If set to false, the diagnostic submission and app analytics settings in the Diagnostics & Usage pane in Settings cannot be modified. Defaults to true .
                    <br>Available in iOS 9.3.2 and later</td>
            </tr>
            <tr>
                <td><strong>Allow notifications modification</strong></td>
                <td>If set to false, notification settings cannot be modified. Defaults to true.
                    <br>Available in iOS 9.3 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow predictive keyboard</strong></td>
                <td>If set to false, disables predictive keyboards. Defaults to true.
                    <br> Available in iOS 8.1.3 and later.</td>
            </tr>
            <tr>
                <td><strong>Force Authentication Before Auto Fill</strong></td>
                <td>If set to true, the user will have to authenticate before passwords or credit card information can be autofilled in Safari and Apps. If this restriction is not enforced, the user can toggle this feature in settings. Only supported on devices with FaceID or TouchID. Defaults to true.
                    <br> Available only in iOS 11.0 and later</td>
            </tr>
            <tr>
                <td><strong><center>Restrictions on mac OS device</center></strong></td>
            </tr>
            <tr>
                <td><strong>Allow macOS iCloud Bookmark sync</strong></td>
                <td>When false, disallows macOS iCloud Bookmark sync.
                    <br> Available in macOS 10 .12 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow macOS Mail iCloud services</strong></td>
                <td>When false, disallows macOS Mail iCloud services.
                    <br> Available in macOS 10 .12 and later</td>
            </tr>
            <tr>
                <td><strong>Allow macOS Mail iCloud Calender services</strong></td>
                <td>When false, disallows macOS iCloud Calendar services.
                    <br>Available in macOS 10.12 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow macOS Mail iCloud Reminder services</strong></td>
                <td>When false, disallows iCloud Reminder services.
                    <br> Available in macOS 10 .12 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow macOS Mail iCloud Address Book services</strong>
                    <br>Available in macOS 10.12 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow macOS Mail iCloud Notes services</strong></td>
                <td>When false, disallows macOS iCloud Notes services.
                    <br> Available in macOS 10.12 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow content caching</strong></td>
                <td>When false, this disallows content caching. Defaults to true.
                    <br>Available only in macOS 10.13 and later.</td>
            </tr>
            <tr>
                <td><strong>Allow iTunes application file sharing</strong></td>
                <td>When false, iTunes application file sharing services are disabled.
                    <br>Available in macOS 10.13 and later.</td>
            </tr>
    </tbody>
</table>

{{< hint info >}}
<b> <a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}
