---
bookCollapseSection: true
weight: 14
---

# Certificate Install

{{< hint info >}}
<b> <a href ="{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an iOS device.
{{< /hint >}}

This configurations can be used to install certificate on an iOS device.

<i>Please note that * sign represents required fields of data.</i>

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
        </tr>
        <tr>
            <td><strong>Certificate name</strong></td>
            <td>The file name of the enclosed certificate.
            </td>
        </tr>
        <tr>
            <td><strong>Certificate file</strong></td>
            <td>The base64 representation of the payload with a line length of 52.</td>
        </tr>
        <tr>
            <td><strong>Certificate Password </strong></td>
            <td> For PKCS#12 certificates, contains the password to the identity.</td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Certificate type</strong>
                    <br>The PayloadType of a certificate payload must be one of the following:</center>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table style="width: 100%;">
                    <tbody>
                        <tr>
                            <th>Payload type</th>
                            <th>Container format</th>
                            <th>Certificate type</th>
                        </tr>
                        <tr>
                            <td>com.apple.security.root</td>
                            <td>PKCS#1(.cer)</td>
                            <td>Alias for com.apple.security.pkcs1.</td>
                        </tr>
                        <tr>
                            <td>com.apple.security.pkcs1 </td>
                            <td>PKCS#1(.cer)</td>
                            <td>DER-encoded certificate without private key. May contain root certificates.</td>
                        </tr>
                        <tr>
                            <td>com.apple.security.pem</td>
                            <td>PKCS#1(.cer)</td>
                            <td>PEM-encoded certificate without private key. May contain root certificates</td>
                        </tr>
                        <tr>
                            <td>com.apple.security.pkcs12</td>
                            <td>PKCS#12(.p12)</td>
                            <td>Password-protected identity certificate. Only one certificate may be included.</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>

{{< hint info >}}
<b> <a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}
